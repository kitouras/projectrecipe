package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Recipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.RecipeRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class RecipesByProductsControllerTest {

    private static final Recipe r1 = Recipe.create(new BigDecimal(1), "r1", "d1");
    private static final Recipe r2 = Recipe.create(new BigDecimal(1), "r2", "d2");
    private static final Product p1 = Product.create(new BigDecimal(1), "p1");
    private static final Product p2 = Product.create(new BigDecimal(2), "p2");

    @Mock
    private ProductRepository productRepository;
    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private ProductService productService;
    @InjectMocks
    private RecipeService recipeService;

    private RecipesByProductsController recipesByProductsController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getRecipesByProducts() {
        recipesByProductsController = new RecipesByProductsController(recipeService, productService);
        when(recipeRepository.getRecipesByProducts(eq(Arrays.asList("p1", "p2")))).thenReturn(Arrays.asList(r1, r2));
        List<Recipe> rbp1 = recipesByProductsController.getRecipesByProducts(Arrays.asList("p1", "p2"));
        assertThat(rbp1.size(), is(equalTo(2)));
        assertThat(rbp1.get(0).getId(), is(equalTo(new BigDecimal(1))));
        assertThat(rbp1.get(1).getName(), is(equalTo("r2")));
        assertThat(rbp1.get(1).getDescription(), is(equalTo("d2")));

        Mockito.verify(recipeRepository, Mockito.times(1)).getRecipesByProducts(eq(Arrays.asList("p1", "p2")));
    }

    @Test
    void getAllProducts() {
        recipesByProductsController = new RecipesByProductsController(recipeService, productService);
        Mockito.when(productRepository.findAll()).thenReturn(Arrays.asList(p1, p2));
        List<String> l_p = recipesByProductsController.getAllProducts();

        assertThat(l_p.size(), is(equalTo(2)));
        assertThat(l_p.get(0), is(equalTo(p1.getName())));
        Mockito.verify(productRepository, Mockito.times(1)).findAll();

    }
}