package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.LoginResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.RecipeAppUserRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeAppUserService;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class UserControllerTest {

    private static RecipeAppUser u1;
    private static RecipeAppUser u2;
    private static RecipeAppUser u2Empty;
    private static LoginPassword lp1;
    private static LoginPassword lp2;

    private UserController userController;

    @Mock
    private RecipeAppUserRepository recipeAppUserRepository;

    @InjectMocks
    private RecipeAppUserService recipeAppUserService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeAll
    public static void init() {
        u1 = new RecipeAppUser();
        u1.setId(new BigDecimal(1));
        u1.setLogin("l1");
        u1.setPassword("p1");
        u1.setRole("r1");

        u2 = new RecipeAppUser();
        u2.setId(new BigDecimal(2));
        u2.setLogin("l2");
        u2.setPassword("p2");
        u2.setRole("U");

        u2Empty = new RecipeAppUser();
        u2Empty.setLogin("l2");
        u2Empty.setPassword("p2");
        u2Empty.setRole("U");

        lp1 = new LoginPassword();
        lp1.setLogin("l1");
        lp1.setPassword("p1");

        lp2 = new LoginPassword();
        lp2.setLogin("l2");
        lp2.setPassword("p2");
    }

    @Test
    void logIn() {
        userController = new UserController(recipeAppUserService);

        Mockito.when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(Mockito.eq("l1"), Mockito.eq("p1"))).thenReturn(u1);
        LoginResponse lr = userController.logIn("l1", "p1");
        assertThat(lr.getState(), is("Y"));
        assertThat(lr.getLogin(), is("l1"));
        assertThat(lr.getPassword(), is("p1"));
        assertThat(lr.getRole(), is("r1"));

        Mockito.when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(Mockito.eq("l2"), Mockito.eq("p2"))).thenReturn(null);
        lr = userController.logIn("l2", "p2");
        assertThat(lr.getState(), is(equalTo("N")));

        Mockito.verify(recipeAppUserRepository, Mockito.times(1)).getRecipeAppUserByLoginAndPassword("l1", "p1");
        Mockito.verify(recipeAppUserRepository, Mockito.times(1)).getRecipeAppUserByLoginAndPassword("l2", "p2");

    }

    @Test
    void register() {
        userController = new UserController(recipeAppUserService);

        Mockito.when(recipeAppUserRepository.findRecipeAppUserByLogin(Mockito.eq("l1"))).thenReturn(u1);
        assertThat(userController.register(lp1), is(equalTo(false)));

        Mockito.when(recipeAppUserRepository.findRecipeAppUserByLogin(Mockito.eq("l2"))).thenReturn(null);
        Mockito.when(recipeAppUserRepository.saveAndFlush(Mockito.eq(u2Empty))).thenReturn(u2);
        assertThat(userController.register(lp2), is(equalTo(true)));

        Mockito.verify(recipeAppUserRepository, Mockito.times(1)).findRecipeAppUserByLogin("l2");
        Mockito.verify(recipeAppUserRepository, Mockito.times(1)).findRecipeAppUserByLogin("l2");
        Mockito.verify(recipeAppUserRepository, Mockito.times(1)).saveAndFlush(u2Empty);
    }
}