package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.NumberOfRecipesByModerators;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ClientOfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.OfferingRecipeResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.*;
import ru.nsu.ccfit.vershinin.projectrecipe.services.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

class RecipesControllerTest {

    private static final NumberOfRecipesByModerators norbm3 = NumberOfRecipesByModerators.create(new BigDecimal(3), new BigDecimal(3));
    private static final NumberOfRecipesByModerators norbm4 = NumberOfRecipesByModerators.create(new BigDecimal(4), new BigDecimal(4));
    private static final RecipeAppUser u4 = RecipeAppUser.create(new BigDecimal(4), "l4", "p4", "M");
    private static OfferingRecipe or4;
    private static Recipe r4;
    private static final NumberOfRecipesByModerators norbm5 = NumberOfRecipesByModerators.create(new BigDecimal(5), new BigDecimal(5));
    private static final RecipeAppUser u5 = RecipeAppUser.create(new BigDecimal(5), "l5", "p5", "M");
    private static OfferingRecipe or5;
    private static Recipe r5;
    private static final NumberOfRecipesByModerators norbm6 = NumberOfRecipesByModerators.create(new BigDecimal(6), new BigDecimal(6));
    private static final RecipeAppUser u6 = RecipeAppUser.create(new BigDecimal(6), "l6", "p6", "M");
    private static OfferingRecipe or6;
    private static Recipe r6;

    private static final OfferingRecipe or7 = OfferingRecipe.create(new BigDecimal(4), "n4", "d4", u4);


    @Mock
    private RecipeAppUserRepository recipeAppUserRepository;
    @Mock
    private RecipeRepository recipeRepository;
    @Mock
    private OfferingRecipeRepository offeringRecipeRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductOfferingRecipeRepository productOfferingRecipeRepository;
    @Mock
    private ProductRecipeRepository productRecipeRepository;

    @InjectMocks
    private RecipeAppUserService recipeAppUserService;
    @InjectMocks
    private RecipeService recipeService;
    @InjectMocks
    private OfferingRecipeService offeringRecipeService;
    @InjectMocks
    private ProductService productService;
    @InjectMocks
    private ProductOfferingRecipeService productOfferingRecipeService;
    @InjectMocks
    private ProductRecipeService productRecipeService;

    private RecipesController recipesController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeAll
    public static void init() {
        or4 = new OfferingRecipe();
        or4.setName("n4");
        or4.setDescription("d4");
        or4.setRecipeAppUser(u4);

        r4 = new Recipe();
        r4.setName("n4");
        r4.setDescription("d4");

        or5 = new OfferingRecipe();
        or5.setName("n5");
        or5.setDescription("d5");
        or5.setRecipeAppUser(u5);

        r5 = new Recipe();
        r5.setName("n5");
        r5.setDescription("d5");

        or6 = new OfferingRecipe();
        or6.setName("n6");
        or6.setDescription("d6");
        or6.setRecipeAppUser(u6);

        r6 = new Recipe();
        r6.setName("n6");
        r6.setDescription("d6");

    }

    @Test
    void offerRecipe() {
        recipesController = new RecipesController(
                recipeAppUserService,
                recipeService,
                offeringRecipeService,
                productService,
                productOfferingRecipeService,
                productRecipeService);

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l1"), eq("p1"))).thenReturn(null);
        boolean o1 = recipesController.offerRecipe(ClientOfferingRecipe.create("n1", Collections.emptyList(), "d1", "l1", "p1"));
        assertThat(o1, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l2"), eq("p2"))).thenReturn(RecipeAppUser.create(new BigDecimal(2), "l2", "p2", "U"));
        when(offeringRecipeRepository.getNumberOfRecipesByModerators()).thenReturn(null);
        boolean o2 = recipesController.offerRecipe(ClientOfferingRecipe.create("n2", Collections.emptyList(), "d2", "l2", "p2"));
        assertThat(o2, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l3"), eq("p3"))).thenReturn(RecipeAppUser.create(new BigDecimal(3), "l3", "p3", "M"));
        when(offeringRecipeRepository.getNumberOfRecipesByModerators()).thenReturn(Collections.singletonList(norbm3));
        when(recipeAppUserRepository.findById(eq(norbm3.getModeratorId()))).thenReturn(Optional.empty());
        boolean o3 = recipesController.offerRecipe(ClientOfferingRecipe.create("n3", Collections.emptyList(), "d3", "l3", "p3"));
        assertThat(o3, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l4"), eq("p4"))).thenReturn(RecipeAppUser.create(new BigDecimal(4), "l4", "p4", "M"));
        when(offeringRecipeRepository.getNumberOfRecipesByModerators()).thenReturn(Collections.singletonList(norbm4));
        when(recipeAppUserRepository.findById(eq(norbm4.getModeratorId()))).thenReturn(Optional.of(u4));
        when(offeringRecipeRepository.saveAndFlush(eq(or4))).thenReturn(new OfferingRecipe());
        boolean o4 = recipesController.offerRecipe(ClientOfferingRecipe.create("n4", Collections.emptyList(), "d4", "l4", "p4"));
        assertThat(o4, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l5"), eq("p5"))).thenReturn(RecipeAppUser.create(new BigDecimal(5), "l5", "p5", "M"));
        when(offeringRecipeRepository.getNumberOfRecipesByModerators()).thenReturn(Collections.singletonList(norbm5));
        when(recipeAppUserRepository.findById(eq(norbm5.getModeratorId()))).thenReturn(Optional.of(u5));
        when(offeringRecipeRepository.saveAndFlush(eq(or5))).thenReturn(OfferingRecipe.create(new BigDecimal(5), or5.getName(), or5.getDescription(), or5.getRecipeAppUser()));
        when(productRepository.getProductByName(eq("p5"))).thenReturn(Product.create(new BigDecimal(5), "p5"));
        when(productOfferingRecipeRepository
                .saveAndFlush(
                        eq(ProductOfferingRecipe.create(ProductOfferingRecipe
                                .ProductOfferingRecipeId.create(
                                        Product.create(new BigDecimal(5), "p5"),
                                        OfferingRecipe.create(new BigDecimal(5), or5.getName(), or5.getDescription(), or5.getRecipeAppUser()))))))
                .thenReturn(new ProductOfferingRecipe());
        boolean o5 = recipesController.offerRecipe(ClientOfferingRecipe.create("n5", Collections.singletonList("p5"), "d5", "l5", "p5"));
        assertThat(o5, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l6"), eq("p6"))).thenReturn(RecipeAppUser.create(new BigDecimal(6), "l6", "p6", "M"));
        when(offeringRecipeRepository.getNumberOfRecipesByModerators()).thenReturn(Collections.singletonList(norbm6));
        when(recipeAppUserRepository.findById(eq(norbm6.getModeratorId()))).thenReturn(Optional.of(u6));
        when(offeringRecipeRepository.saveAndFlush(eq(or6))).thenReturn(OfferingRecipe.create(new BigDecimal(6), or6.getName(), or6.getDescription(), or6.getRecipeAppUser()));
        when(productRepository.getProductByName(eq("p6"))).thenReturn(Product.create(new BigDecimal(6), "p6"));
        when(productOfferingRecipeRepository
                .saveAndFlush(
                        eq(ProductOfferingRecipe.create(ProductOfferingRecipe
                                .ProductOfferingRecipeId.create(
                                        Product.create(new BigDecimal(6), "p6"),
                                        OfferingRecipe.create(new BigDecimal(6), or6.getName(), or6.getDescription(), or6.getRecipeAppUser()))))))
                .thenReturn(ProductOfferingRecipe.create(new ProductOfferingRecipe.ProductOfferingRecipeId()));
        boolean o6 = recipesController.offerRecipe(ClientOfferingRecipe.create("n6", Collections.singletonList("p6"), "d6", "l6", "p6"));
        assertThat(o6, is(equalTo(true)));
    }

    @Test
    void getOfferingRecipesByModerator() {
        recipesController = new RecipesController(
                recipeAppUserService,
                recipeService,
                offeringRecipeService,
                productService,
                productOfferingRecipeService,
                productRecipeService);
        when(offeringRecipeRepository.getOfferingRecipesByRecipeAppUser_LoginAndRecipeAppUser_Password(eq("l1"), eq("p1"))).thenReturn(null);
        List<OfferingRecipeResponse> lorr1 = recipesController.getOfferingRecipesByModerator("l1", "p1");
        assertThat(lorr1, is(equalTo(Collections.emptyList())));

        when(offeringRecipeRepository
                .getOfferingRecipesByRecipeAppUser_LoginAndRecipeAppUser_Password(
                        eq("l4"),
                        eq("p4")))
                .thenReturn(Collections.singletonList(or7));
        when(productOfferingRecipeRepository
                .getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(eq(or7)))
                .thenReturn(Collections.singletonList(ProductOfferingRecipe.create(
                        ProductOfferingRecipe.ProductOfferingRecipeId.create(Product.create(new BigDecimal(4), "p4"), or7))));
        List<OfferingRecipeResponse> lorr4 = recipesController.getOfferingRecipesByModerator("l4", "p4");
        assertThat(lorr4.size(), is(equalTo(1)));
        assertThat(lorr4.get(0).getName(), is(equalTo("n4")));

    }

    @Test
    void approveOfferingRecipe() {
        recipesController = new RecipesController(
                recipeAppUserService,
                recipeService,
                offeringRecipeService,
                productService,
                productOfferingRecipeService,
                productRecipeService);
        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l1"), eq("p1"))).thenReturn(null);
        boolean a1 = recipesController.approveOfferingRecipe("1", LoginPassword.create("l1", "p1"));
        assertThat(a1, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l2"), eq("p2"))).thenReturn(RecipeAppUser.create(new BigDecimal(2), "l2", "p2", "U"));
        boolean a2 = recipesController.approveOfferingRecipe("2", LoginPassword.create("l2", "p2"));
        assertThat(a2, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l3"), eq("p3"))).thenReturn(RecipeAppUser.create(new BigDecimal(3), "l3", "p3", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(3)))).thenReturn(Optional.empty());
        boolean a3 = recipesController.approveOfferingRecipe("3", LoginPassword.create("l3", "p3"));
        assertThat(a3, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l4"), eq("p4"))).thenReturn(RecipeAppUser.create(new BigDecimal(4), "l4", "p4", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(4)))).thenReturn(Optional.of(or7));
        when(recipeRepository.saveAndFlush(eq(r4))).thenReturn(new Recipe());
        boolean a4 = recipesController.approveOfferingRecipe("4", LoginPassword.create("l4", "p4"));
        assertThat(a4, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l5"), eq("p5"))).thenReturn(RecipeAppUser.create(new BigDecimal(5), "l5", "p5", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(5)))).thenReturn(Optional.of(OfferingRecipe.create(new BigDecimal(5), "n5", "d5", u5)));
        when(recipeRepository.saveAndFlush(eq(r5))).thenReturn(Recipe.create(new BigDecimal(5), "n5", "d5"));
        when(productOfferingRecipeRepository
                .getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(
                        eq(OfferingRecipe.create(
                                new BigDecimal(5),
                                "n5",
                                "d5", u5))))
                .thenReturn(Collections.singletonList(ProductOfferingRecipe.create(ProductOfferingRecipe.ProductOfferingRecipeId.create(Product.create(new BigDecimal(5), "p5"), OfferingRecipe.create(new BigDecimal(5), "n5", "d5", u5)))));
        when(productRecipeRepository
                .saveAndFlush(eq(ProductRecipe
                        .create(ProductRecipe.ProductRecipeId
                                .create(
                                        Product.create(new BigDecimal(5), "p5"),
                                        Recipe.create(new BigDecimal(5), "n5", "d5"))))))
                .thenReturn(new ProductRecipe());
        boolean a5 = recipesController.approveOfferingRecipe("5", LoginPassword.create("l5", "p5"));
        assertThat(a5, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l6"), eq("p6"))).thenReturn(RecipeAppUser.create(new BigDecimal(6), "l6", "p6", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(6)))).thenReturn(Optional.of(OfferingRecipe.create(new BigDecimal(6), "n6", "d6", u6)));
        when(recipeRepository.saveAndFlush(eq(r6))).thenReturn(Recipe.create(new BigDecimal(6), "n6", "d6"));
        when(productOfferingRecipeRepository
                .getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(
                        eq(OfferingRecipe.create(
                                new BigDecimal(6),
                                "n6",
                                "d6", u6))))
                .thenReturn(Collections.singletonList(ProductOfferingRecipe.create(ProductOfferingRecipe.ProductOfferingRecipeId.create(Product.create(new BigDecimal(6), "p6"), OfferingRecipe.create(new BigDecimal(6), "n6", "d6", u6)))));
        when(productRecipeRepository
                .saveAndFlush(eq(ProductRecipe
                        .create(ProductRecipe.ProductRecipeId
                                .create(
                                        Product.create(new BigDecimal(6), "p6"),
                                        Recipe.create(new BigDecimal(6), "n6", "d6"))))))
                .thenReturn(ProductRecipe.create(ProductRecipe.ProductRecipeId.create(Product.create(new BigDecimal(6), "p6"), Recipe.create(new BigDecimal(6), "n6", "d6"))));
        boolean a6 = recipesController.approveOfferingRecipe("6", LoginPassword.create("l6", "p6"));
        assertThat(a6, is(equalTo(true)));
    }

    @Test
    void rejectOfferingRecipe() {
        recipesController = new RecipesController(
                recipeAppUserService,
                recipeService,
                offeringRecipeService,
                productService,
                productOfferingRecipeService,
                productRecipeService);
        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l1"), eq("p1"))).thenReturn(null);
        boolean a1 = recipesController.rejectOfferingRecipe("1", LoginPassword.create("l1", "p1"));
        assertThat(a1, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l2"), eq("p2"))).thenReturn(RecipeAppUser.create(new BigDecimal(2), "l2", "p2", "U"));
        boolean a2 = recipesController.rejectOfferingRecipe("2", LoginPassword.create("l2", "p2"));
        assertThat(a2, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l3"), eq("p3"))).thenReturn(RecipeAppUser.create(new BigDecimal(3), "l3", "p3", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(3)))).thenReturn(Optional.empty());
        boolean a3 = recipesController.rejectOfferingRecipe("3", LoginPassword.create("l3", "p3"));
        assertThat(a3, is(equalTo(false)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l4"), eq("p4"))).thenReturn(RecipeAppUser.create(new BigDecimal(4), "l4", "p4", "M"));
        when(offeringRecipeRepository.findById(eq(new BigDecimal(4)))).thenReturn(Optional.of(or7));
        boolean a4 = recipesController.rejectOfferingRecipe("4", LoginPassword.create("l4", "p4"));
        assertThat(a4, is(equalTo(true)));
    }
}