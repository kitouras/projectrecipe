package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductsSetForCreate;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.ProductsSetResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductFromSetRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductsSetRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.RecipeAppUserRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductFromSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductsSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeAppUserService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

class ProductsSetControllerTest {


    private static final RecipeAppUser u2 = RecipeAppUser.create(new BigDecimal(2), "l2", "p2", "U");
    private static final ProductsSet ps2 = ProductsSet.create(new BigDecimal(2), u2, "ps2");
    private static ProductsSet e_ps2;
    private static final ProductsSet ps3 = ProductsSet.create(new BigDecimal(3), u2, "ps3");
    private static final Product p2 = Product.create(new BigDecimal(2), "p2");
    private static final Product p3 = Product.create(new BigDecimal(3), "p3");
    private static final ProductFromSet pfs2 = ProductFromSet.create(ProductFromSet.ProductFromSetId.create(ps2, p2));
    private static final ProductFromSet pfs3 = ProductFromSet.create(ProductFromSet.ProductFromSetId.create(ps2, p3));
    private static final ProductsSetForCreate psfc2 = ProductsSetForCreate.create("ps2", Arrays.asList("p2", "p3"), "l2", "p2");


    private ProductsSetController productsSetController;

    @Mock
    private RecipeAppUserRepository recipeAppUserRepository;
    @Mock
    private ProductsSetRepository productsSetRepository;
    @Mock
    private ProductFromSetRepository productFromSetRepository;
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private RecipeAppUserService recipeAppUserService;
    @InjectMocks
    private ProductsSetService productsSetService;
    @InjectMocks
    private ProductFromSetService productFromSetService;
    @InjectMocks
    private ProductService productService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeAll
    public static void init() {
        e_ps2 = new ProductsSet();
        e_ps2.setName("ps2");
        e_ps2.setRecipeAppUser(u2);
    }

    @Test
    void getProductsSets() {
        productsSetController = new ProductsSetController(recipeAppUserService, productsSetService, productFromSetService, productService);
        when(productsSetRepository.getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(eq("l1"), eq("p1"))).thenReturn(null);
        List<ProductsSetResponse> psrs1 = productsSetController.getProductsSets("l1", "p1");
        assertThat(psrs1.size(), is(equalTo(0)));

        when(productsSetRepository.getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(eq("l2"), eq("p2"))).thenReturn(Arrays.asList(ps2, ps3));
        when(productFromSetRepository.getProductsByProductsSet(eq(ps2.getId()))).thenReturn(Collections.singletonList(p2));
        when(productFromSetRepository.getProductsByProductsSet(eq(ps3.getId()))).thenReturn(Collections.singletonList(p3));
        List<ProductsSetResponse> psrs2 = productsSetController.getProductsSets("l2", "p2");
        assertThat(psrs2.size(), is(equalTo(2)));
        assertThat(psrs2.get(0).getProducts().get(0).getId(), is(equalTo(p2.getId())));
        assertThat(psrs2.get(1).getName(), is(equalTo(ps3.getName())));

        verify(productsSetRepository, times(1)).getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(eq("l1"), eq("p1"));

        verify(productsSetRepository, times(1)).getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(eq("l2"), eq("p2"));
        verify(productFromSetRepository, times(1)).getProductsByProductsSet(eq(ps2.getId()));
        verify(productFromSetRepository, times(1)).getProductsByProductsSet(eq(ps3.getId()));

    }

    @Test
    void deleteProductSet() {
        productsSetController = new ProductsSetController(recipeAppUserService, productsSetService, productFromSetService, productService);
        when(productsSetRepository
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        eq(new BigDecimal(1)),
                        eq("l1"),
                        eq("p1")))
                .thenReturn(null);
        boolean d1 = productsSetController.deleteProductSet("1", LoginPassword.create("l1", "p1"));
        assertThat(d1, is(equalTo(true)));

        when(productsSetRepository
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        eq(new BigDecimal(2)),
                        eq("l2"),
                        eq("p2")))
                .thenReturn(ps2);
        boolean d2 = productsSetController.deleteProductSet("2", LoginPassword.create("l2", "p2"));
        assertThat(d2, is(equalTo(true)));

        verify(productsSetRepository, times(1)).getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                eq(new BigDecimal(1)),
                eq("l1"),
                eq("p1"));

        verify(productsSetRepository, times(1)).getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                eq(new BigDecimal(2)),
                eq("l2"),
                eq("p2"));
    }

    @Test
    void addProductsSets() {
        productsSetController = new ProductsSetController(recipeAppUserService, productsSetService, productFromSetService, productService);
        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l1"), eq("p1"))).thenReturn(null);
        ProductsSetResponse psr1 = productsSetController.addProductsSets(ProductsSetForCreate.create("ps1", Collections.emptyList(), "l1", "p1"));
        assertThat(psr1.getId(), is(equalTo(null)));

        when(recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(eq("l2"), eq("p2"))).thenReturn(u2);
        when(productsSetRepository.saveAndFlush(eq(e_ps2))).thenReturn(ps2);
        when(productRepository.getProductByName(eq("p2"))).thenReturn(p2);
        when(productFromSetRepository.saveAndFlush(eq(pfs2))).thenReturn(pfs2);
        when(productRepository.getProductByName(eq("p3"))).thenReturn(p3);
        when(productFromSetRepository.saveAndFlush(eq(pfs3))).thenReturn(pfs3);
        ProductsSetResponse psr2 = productsSetController.addProductsSets(psfc2);
        assertThat(psr2.getId(), is(equalTo(ps2.getId())));
        assertThat(psr2.getName(), is(equalTo(ps2.getName())));
        assertThat(psr2.getProducts().get(0).getId(), is(equalTo(p2.getId())));
        assertThat(psr2.getProducts().get(1).getName(), is(equalTo(p3.getName())));

        verify(recipeAppUserRepository, times(1)).getRecipeAppUserByLoginAndPassword(eq("l1"), eq("p1"));

        verify(recipeAppUserRepository, times(1)).getRecipeAppUserByLoginAndPassword(eq("l2"), eq("p2"));
        verify(productsSetRepository, times(1)).saveAndFlush(eq(e_ps2));
        verify(productRepository, times(1)).getProductByName(eq("p2"));
        verify(productFromSetRepository, times(1)).saveAndFlush(eq(pfs2));
        verify(productRepository, times(1)).getProductByName(eq("p3"));
        verify(productFromSetRepository, times(1)).saveAndFlush(eq(pfs3));
    }
}