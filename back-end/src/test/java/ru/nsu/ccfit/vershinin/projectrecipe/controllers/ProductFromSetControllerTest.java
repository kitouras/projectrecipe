package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductsForAdding;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.ProductsSetResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductFromSetRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductsSetRepository;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductFromSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductsSetService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

class ProductFromSetControllerTest {

    private static ProductsForAdding pfa1;

    private static final Product p2 = Product.create(new BigDecimal(2), "p2");
    private static final ProductsForAdding pfa2 = ProductsForAdding.create(
            new BigDecimal(2),
            Collections.singletonList("p2"),
            "l2",
            "p2"
    );
    private static final RecipeAppUser u2 = RecipeAppUser.create(
            new BigDecimal(1),
            "l2",
            "p2",
            "U"
    );
    private static final ProductsSet ps2 = ProductsSet.create(
            new BigDecimal(2),
            u2,
            "ps2"
    );
    private static final ProductFromSet pfs2 = ProductFromSet.create(
            ProductFromSet.ProductFromSetId.create(
                    ps2,
                    p2
            )
    );

    private ProductFromSetController productFromSetController;

    @Mock
    private ProductFromSetRepository productFromSetRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductsSetRepository productsSetRepository;

    @InjectMocks
    private ProductFromSetService productFromSetService;
    @InjectMocks
    private ProductService productService;
    @InjectMocks
    private ProductsSetService productsSetService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeAll
    public static void init() {

        pfa1 = new ProductsForAdding();
        pfa1.setSetId(new BigDecimal(1));
        pfa1.setLogin("l1");
        pfa1.setPassword("p1");

        Product p2 = new Product();
        p2.setId(new BigDecimal(2));
        p2.setName("p2");

    }

    @Test
    void addProductsIntoSet() {
        productFromSetController = new ProductFromSetController(
                productFromSetService,
                productService,
                productsSetService
        );

        Mockito.when(
                productsSetRepository
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(1)),
                        Mockito.eq("l1"),
                        Mockito.eq("p1")))
                .thenReturn(null);
        ProductsSetResponse psr1 = productFromSetController.addProductsIntoSet(pfa1);
        assertThat(psr1.getId(), is(equalTo(null)));

        Mockito.when(
                productsSetRepository
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                                Mockito.eq(new BigDecimal(2)),
                                Mockito.eq("l2"),
                                Mockito.eq("p2")))
                .thenReturn(ps2);
        Mockito.when(productRepository.getProductByName(Mockito.eq("p2"))).thenReturn(p2);
        Mockito.when(productFromSetRepository.saveAndFlush(Mockito.eq(pfs2))).thenReturn(pfs2);
        Mockito.when(productFromSetRepository.getProductsByProductsSet(Mockito.eq(new BigDecimal(2)))).thenReturn(Collections.singletonList(p2));
        ProductsSetResponse psr2 = productFromSetController.addProductsIntoSet(pfa2);
        assertThat(psr2.getId(), is(equalTo(pfa2.getSetId())));
        assertThat(psr2.getName(), is(equalTo(ps2.getName())));
        assertThat(psr2.getProducts().size(), is(equalTo(1)));
        assertThat(psr2.getProducts().get(0).getId(), is(equalTo(p2.getId())));

        Mockito.verify(productsSetRepository, Mockito.times(1))
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(1)),
                        Mockito.eq("l1"),
                        Mockito.eq("p1")
                );

        Mockito.verify(productsSetRepository, Mockito.times(1))
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(2)),
                        Mockito.eq("l2"),
                        Mockito.eq("p2")
                );
        Mockito.verify(productRepository, Mockito.times(1)).getProductByName(Mockito.eq("p2"));
        Mockito.verify(productFromSetRepository, Mockito.times(1)).saveAndFlush(Mockito.eq(pfs2));
        Mockito.verify(productFromSetRepository, Mockito.times(1)).getProductsByProductsSet(Mockito.eq(new BigDecimal(2)));
    }

    @Test
    void deleteProductFromSet() {
        productFromSetController = new ProductFromSetController(
                productFromSetService,
                productService,
                productsSetService
        );

        Mockito.when(
                productsSetRepository
                        .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                                Mockito.eq(new BigDecimal(1)),
                                Mockito.eq("l1"),
                                Mockito.eq("p1")))
                .thenReturn(null);
        ProductsSetResponse psr1 = productFromSetController.deleteProductFromSet("1", "1", LoginPassword.create("l1", "p1"));
        assertThat(psr1.getId(), is(equalTo(null)));

        Mockito.when(
                productsSetRepository
                        .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                                Mockito.eq(new BigDecimal(2)),
                                Mockito.eq("l2"),
                                Mockito.eq("p2")))
                .thenReturn(ps2);
        Mockito.when(productRepository.findById(Mockito.eq(new BigDecimal(2)))).thenReturn(Optional.of(p2));
        Mockito.when(productFromSetRepository.getProductsByProductsSet(Mockito.eq(new BigDecimal(2)))).thenReturn(Collections.emptyList());
        ProductsSetResponse psr2 = productFromSetController.deleteProductFromSet("2", "2", LoginPassword.create("l2", "p2"));
        assertThat(psr2.getId(), is(equalTo(ps2.getId())));
        assertThat(psr2.getName(), is(equalTo(ps2.getName())));
        assertThat(psr2.getProducts().size(), is(equalTo(0)));


        Mockito.when(
                productsSetRepository
                        .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                                Mockito.eq(new BigDecimal(3)),
                                Mockito.eq("l3"),
                                Mockito.eq("p3")))
                .thenReturn(new ProductsSet());
        Mockito.when(productRepository.findById(Mockito.eq(new BigDecimal(3)))).thenReturn(Optional.empty());
        ProductsSetResponse psr3 = productFromSetController.deleteProductFromSet("3", "3", LoginPassword.create("l3", "p3"));
        assertThat(psr3.getId(), is(equalTo(null)));

        Mockito.verify(productsSetRepository, Mockito.times(1))
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(1)),
                        Mockito.eq("l1"),
                        Mockito.eq("p1")
                );

        Mockito.verify(productsSetRepository, Mockito.times(1))
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(2)),
                        Mockito.eq("l2"),
                        Mockito.eq("p2")
                );
        Mockito.verify(productRepository, Mockito.times(1)).findById(Mockito.eq(new BigDecimal(2)));
        Mockito.verify(productFromSetRepository, Mockito.times(1))
                .delete(Mockito.eq(ProductFromSet.create(ProductFromSet.ProductFromSetId.create(ps2, p2))));
        Mockito.verify(productFromSetRepository, Mockito.times(1)).getProductsByProductsSet(Mockito.eq(new BigDecimal(2)));

        Mockito.verify(productsSetRepository, Mockito.times(1))
                .getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(
                        Mockito.eq(new BigDecimal(3)),
                        Mockito.eq("l3"),
                        Mockito.eq("p3")
                );
        Mockito.verify(productRepository, Mockito.times(1)).findById(Mockito.eq(new BigDecimal(3)));
    }
}