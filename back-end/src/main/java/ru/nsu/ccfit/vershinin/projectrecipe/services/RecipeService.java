package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Recipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.RecipeRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RecipeService {

    @Autowired
    private RecipeRepository recipeRepository;

    @Transactional
    public List<Recipe> getRecipesByProducts(List<String> products) {
        return recipeRepository.getRecipesByProducts(products);
    }

    @Transactional
    public Recipe saveRecipe(Recipe recipe) {
        return recipeRepository.saveAndFlush(recipe);
    }

    @Transactional
    public void deleteRecipe(Recipe recipe) {
        recipeRepository.delete(recipe);
    }
}
