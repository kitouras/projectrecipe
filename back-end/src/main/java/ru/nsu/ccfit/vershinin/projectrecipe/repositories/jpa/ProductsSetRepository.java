package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductsSetRepository extends JpaRepository<ProductsSet, BigDecimal> {
    List<ProductsSet> getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(String login, String password);

    ProductsSet getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(BigDecimal id, String login, String password);
}
