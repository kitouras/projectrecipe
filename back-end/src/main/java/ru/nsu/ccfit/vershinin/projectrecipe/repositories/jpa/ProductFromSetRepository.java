package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.ProductsByProductsSetRepository;

@Repository
public interface ProductFromSetRepository extends JpaRepository<ProductFromSet, ProductFromSet.ProductFromSetId>, ProductsByProductsSetRepository<Product> {
    void deleteByProductFromSetIdProductsSet(ProductsSet productsSet);
}
