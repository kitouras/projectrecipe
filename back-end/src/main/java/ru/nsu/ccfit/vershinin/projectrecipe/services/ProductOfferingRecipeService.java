package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.OfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductOfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductOfferingRecipeRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductOfferingRecipeService {
    @Autowired
    private ProductOfferingRecipeRepository productOfferingRecipeRepository;

    @Transactional
    public List<String> getProductNamesByOfferingRecipe(OfferingRecipe offeringRecipe) {
        return Objects.requireNonNullElse(productOfferingRecipeRepository
                .getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(offeringRecipe)
                .stream()
                .map(ProductOfferingRecipe::getProductOfferingRecipeId)
                .map(ProductOfferingRecipe.ProductOfferingRecipeId::getProduct)
                .map(Product::getName)
                .collect(Collectors.toList()), Collections.emptyList());
    }

    @Transactional
    public List<Product> getProductsByOfferingRecipe(OfferingRecipe offeringRecipe) {
        return Objects.requireNonNullElse(productOfferingRecipeRepository
                .getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(offeringRecipe)
                .stream()
                .map(ProductOfferingRecipe::getProductOfferingRecipeId)
                .map(ProductOfferingRecipe.ProductOfferingRecipeId::getProduct)
                .collect(Collectors.toList()), Collections.emptyList());
    }

    @Transactional
    public ProductOfferingRecipe saveProductByOfferingRecipe(ProductOfferingRecipe productOfferingRecipe) {
        return productOfferingRecipeRepository.saveAndFlush(productOfferingRecipe);
    }

    @Transactional
    public void deleteProductOfferingRecipe(ProductOfferingRecipe productOfferingRecipe) {
        productOfferingRecipeRepository.delete(productOfferingRecipe);
    }

    @Transactional
    public void deleteProductsOfferingRecipesByOfferingRecipe(BigDecimal id) {
        productOfferingRecipeRepository.deleteByProductOfferingRecipeId_OfferingRecipe_Id(id);
    }
}
