package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "RecipeAppUser")
public class RecipeAppUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, precision = 10)
    private BigDecimal id;
    @Column(name = "login", nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(columnDefinition = "CHAR(1)", name = "role", nullable = false)
    private String role;

    @OneToMany(mappedBy = "recipeAppUser", cascade = CascadeType.REMOVE)
    private final Set<OfferingRecipe> offeringRecipes = new HashSet<>();

    @OneToMany(mappedBy = "recipeAppUser", cascade = CascadeType.REMOVE)
    private final Set<ProductsSet> productsSetSet = new HashSet<>();
}
