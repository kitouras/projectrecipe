package ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class ProductsForAdding {
    private BigDecimal setId;
    private List<String> products;
    private String login;
    private String password;
}
