package ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class LoginPassword {
    private String login;
    private String password;
}
