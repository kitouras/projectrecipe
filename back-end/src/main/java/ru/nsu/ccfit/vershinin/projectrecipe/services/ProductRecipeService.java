package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductRecipeRepository;

import javax.transaction.Transactional;

@Service
public class ProductRecipeService {

    @Autowired
    private ProductRecipeRepository productRecipeRepository;

    @Transactional
    public ProductRecipe saveProductRecipe(ProductRecipe productRecipe) {
        return productRecipeRepository.saveAndFlush(productRecipe);
    }

    @Transactional
    public void deleteProductRecipe(ProductRecipe productRecipe) {
        productRecipeRepository.delete(productRecipe);
    }

}
