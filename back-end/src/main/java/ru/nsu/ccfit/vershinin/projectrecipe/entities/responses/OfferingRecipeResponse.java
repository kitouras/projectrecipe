package ru.nsu.ccfit.vershinin.projectrecipe.entities.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class OfferingRecipeResponse {
    private BigDecimal id;
    private String name;
    private List<String> selectedProducts;
    private String description;
}
