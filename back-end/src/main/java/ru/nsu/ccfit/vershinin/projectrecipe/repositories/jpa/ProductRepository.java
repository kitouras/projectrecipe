package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;

import java.math.BigDecimal;

@Repository
public interface ProductRepository extends JpaRepository<Product, BigDecimal> {

    Product getProductByName(String name);
}
