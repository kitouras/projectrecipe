package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public List<String> getAllProducts() {
        return productRepository.findAll().stream().map(Product::getName).collect(Collectors.toList());
    }

    @Transactional
    public Product getProductByName(String name) {
        return productRepository.getProductByName(name);
    }

    @Transactional
    public Optional<Product> getProductById(BigDecimal id) {
        return productRepository.findById(id);
    }
}
