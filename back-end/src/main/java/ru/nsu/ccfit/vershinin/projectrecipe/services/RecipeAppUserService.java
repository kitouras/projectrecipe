package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.RecipeAppUserRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

@Service
public class RecipeAppUserService {

    @Autowired
    private RecipeAppUserRepository recipeAppUserRepository;

    @Transactional
    public RecipeAppUser getUserByNameAndPassword(String login, String password) {
        return recipeAppUserRepository.getRecipeAppUserByLoginAndPassword(login, password);
    }

    @Transactional
    public RecipeAppUser getRecipeAppUserByLogin(String login) {
        return recipeAppUserRepository.findRecipeAppUserByLogin(login);
    }

    @Transactional
    public Optional<RecipeAppUser> getRecipeAppUserById(BigDecimal id) {
        return recipeAppUserRepository.findById(id);
    }

    @Transactional
    public RecipeAppUser saveUser(RecipeAppUser recipeAppUser) {
        return recipeAppUserRepository.saveAndFlush(recipeAppUser);
    }
}
