package ru.nsu.ccfit.vershinin.projectrecipe.entities.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class LoginResponse {
    private String login;
    private String password;
    private String role;
    private String state;
}
