package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.NumberOfRecipesByModerators;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ClientOfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.OfferingRecipeResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.services.*;

import java.math.BigDecimal;
import java.util.*;

@RestController
@RequestMapping("/")
public class RecipesController {

    private final RecipeAppUserService recipeAppUserService;
    private final RecipeService recipeService;
    private final OfferingRecipeService offeringRecipeService;
    private final ProductService productService;
    private final ProductOfferingRecipeService productOfferingRecipeService;
    private final ProductRecipeService productRecipeService;

    public RecipesController(RecipeAppUserService recipeAppUserService, RecipeService recipeService, OfferingRecipeService offeringRecipeService, ProductService productService, ProductOfferingRecipeService productOfferingRecipeService, ProductRecipeService productRecipeService) {
        this.recipeAppUserService = recipeAppUserService;
        this.recipeService = recipeService;
        this.offeringRecipeService = offeringRecipeService;
        this.productService = productService;
        this.productOfferingRecipeService = productOfferingRecipeService;
        this.productRecipeService = productRecipeService;
    }

    @PostMapping("/offer")
    public boolean offerRecipe(@RequestBody ClientOfferingRecipe clientOfferingRecipe) {
        RecipeAppUser userByNameAndPassword = recipeAppUserService.getUserByNameAndPassword(clientOfferingRecipe.getLogin(), clientOfferingRecipe.getPassword());
        if (userByNameAndPassword == null) {
            return false;
        }

        List<NumberOfRecipesByModerators> numberOfRecipesByModerators = offeringRecipeService.getNumberOfRecipesByModerators();
        if (numberOfRecipesByModerators == null) {
            return false;
        }

        Optional<NumberOfRecipesByModerators> minNumberOfRecipes = numberOfRecipesByModerators.stream().min(Comparator.comparing(NumberOfRecipesByModerators::getCountRecipes));
        BigDecimal moderatorId;
        if (minNumberOfRecipes.isPresent()) {
            moderatorId = minNumberOfRecipes.get().getModeratorId();
        } else {
            return false;
        }

        Optional<RecipeAppUser> recipeAppUserById = recipeAppUserService.getRecipeAppUserById(moderatorId);

        OfferingRecipe entityOfferingRecipe = new OfferingRecipe();
        entityOfferingRecipe.setName(clientOfferingRecipe.getName());
        entityOfferingRecipe.setDescription(clientOfferingRecipe.getDescription());

        if (recipeAppUserById.isPresent()) {
            entityOfferingRecipe.setRecipeAppUser(recipeAppUserById.get());
        } else {
            return false;
        }



        OfferingRecipe offeredRecipe = offeringRecipeService.saveOfferingRecipe(entityOfferingRecipe);
        if (offeredRecipe.getId() == null) {
            return false;
        }

        List<String> selectedProducts = clientOfferingRecipe.getSelectedProducts();
        List<ProductOfferingRecipe> savedProducts = new ArrayList<>();
        for (String selectedProductName : selectedProducts) {
            Product productByName = productService.getProductByName(selectedProductName);
            ProductOfferingRecipe.ProductOfferingRecipeId productOfferingRecipeId = new ProductOfferingRecipe.ProductOfferingRecipeId();
            productOfferingRecipeId.setProduct(productByName);
            productOfferingRecipeId.setOfferingRecipe(offeredRecipe);
            ProductOfferingRecipe productOfferingRecipe = new ProductOfferingRecipe();
            productOfferingRecipe.setProductOfferingRecipeId(productOfferingRecipeId);

            ProductOfferingRecipe savingProductOfferingRecipe = productOfferingRecipeService.saveProductByOfferingRecipe(productOfferingRecipe);
            if (savingProductOfferingRecipe.getProductOfferingRecipeId() == null) {
                for (ProductOfferingRecipe savedProduct : savedProducts) {
                    try {
                        productOfferingRecipeService.deleteProductOfferingRecipe(savedProduct);
                    } catch (Exception ignored) {
                        //False exception when there is nothing to delete
                    }
                }
                try {
                    offeringRecipeService.deleteOfferingRecipe(offeredRecipe);
                } catch (Exception ignored) {
                    //False exception when there is nothing to delete
                }
                return false;
            }

            savedProducts.add(savingProductOfferingRecipe);

        }

        return true;
    }

    @GetMapping(path = "/show_offering_recipes")
    public List<OfferingRecipeResponse> getOfferingRecipesByModerator(@RequestParam(name = "login") String login, @RequestParam(name = "password") String password) {
        List<OfferingRecipe> offeringRecipesByUser = offeringRecipeService.getOfferingRecipesByUser(login, password);
        if (offeringRecipesByUser == null) {
            return Collections.emptyList();
        } else {
            List<OfferingRecipeResponse> offeringRecipeResponses = new ArrayList<>();
            for (OfferingRecipe offeringRecipeByUser : offeringRecipesByUser) {
                OfferingRecipeResponse offeringRecipeResponse = new OfferingRecipeResponse();
                offeringRecipeResponse.setId(offeringRecipeByUser.getId());
                offeringRecipeResponse.setName(offeringRecipeByUser.getName());
                offeringRecipeResponse.setDescription(offeringRecipeByUser.getDescription());
                offeringRecipeResponse.setSelectedProducts(productOfferingRecipeService.getProductNamesByOfferingRecipe(offeringRecipeByUser));
                offeringRecipeResponses.add(offeringRecipeResponse);
            }

            return offeringRecipeResponses;
        }
    }

    @PostMapping(path = "/show_offering_recipes/approve/{recipeIdStr}")
    public boolean approveOfferingRecipe(@PathVariable String recipeIdStr, @RequestBody LoginPassword loginPassword) {
        RecipeAppUser userByNameAndPassword = recipeAppUserService.getUserByNameAndPassword(loginPassword.getLogin(), loginPassword.getPassword());
        if (userByNameAndPassword == null || !userByNameAndPassword.getRole().equals("M")) {
            return false;
        }

        BigDecimal recipeId = new BigDecimal(recipeIdStr);
        Optional<OfferingRecipe> getOfferingRecipeById = offeringRecipeService.getOfferingRecipeById(recipeId);
        OfferingRecipe offeringRecipe;
        if (getOfferingRecipeById.isPresent()) {
            offeringRecipe = getOfferingRecipeById.get();
        } else {
            return false;
        }

        Recipe savingRecipe = new Recipe();
        savingRecipe.setName(offeringRecipe.getName());
        savingRecipe.setDescription(offeringRecipe.getDescription());
        Recipe savedRecipe = recipeService.saveRecipe(savingRecipe);
        if (savedRecipe.getId() == null) {
            return false;
        }

        List<Product> productsByOfferingRecipe = productOfferingRecipeService.getProductsByOfferingRecipe(offeringRecipe);
        List<ProductRecipe> savedProductRecipes = new ArrayList<>();
        for (Product product : productsByOfferingRecipe) {
            ProductRecipe.ProductRecipeId savingProductRecipeId = new ProductRecipe.ProductRecipeId();
            ProductRecipe savingProductRecipe = new ProductRecipe();

            savingProductRecipeId.setProduct(product);
            savingProductRecipeId.setRecipe(savedRecipe);
            savingProductRecipe.setProductRecipeId(savingProductRecipeId);

            ProductRecipe savedProductRecipe = productRecipeService.saveProductRecipe(savingProductRecipe);
            if (savedProductRecipe.getProductRecipeId() == null) {
                for (ProductRecipe forDeleteProductRecipe : savedProductRecipes) {
                    try {
                        productRecipeService.deleteProductRecipe(forDeleteProductRecipe);
                    } catch (Exception ignored) {
                        //False exception when there is nothing to delete
                    }
                }
                try {
                    recipeService.deleteRecipe(savedRecipe);
                } catch (Exception ignored) {
                    //False exception when there is nothing to delete
                }
                return false;
            }

            savedProductRecipes.add(savedProductRecipe);

        }

        try {
            productOfferingRecipeService.deleteProductsOfferingRecipesByOfferingRecipe(recipeId);
        } catch (Exception ignored) {
            //False exception when there is nothing to delete
        }

        try {
            offeringRecipeService.deleteOfferingRecipe(offeringRecipe);
        } catch (Exception ignored) {
            //False exception when there is nothing to delete
        }

        return true;
    }

    @DeleteMapping(path = "/show_offering_recipes/reject/{recipeIdStr}")
    public boolean rejectOfferingRecipe(@PathVariable String recipeIdStr, @RequestBody LoginPassword loginPassword) {
        RecipeAppUser userByNameAndPassword = recipeAppUserService.getUserByNameAndPassword(loginPassword.getLogin(), loginPassword.getPassword());
        if (userByNameAndPassword == null || !userByNameAndPassword.getRole().equals("M")) {
            return false;
        }

        BigDecimal recipeId = new BigDecimal(recipeIdStr);
        Optional<OfferingRecipe> getOfferingRecipeById = offeringRecipeService.getOfferingRecipeById(recipeId);
        OfferingRecipe offeringRecipe;
        if (getOfferingRecipeById.isPresent()) {
            offeringRecipe = getOfferingRecipeById.get();
        } else {
            return false;
        }

        try {
            productOfferingRecipeService.deleteProductsOfferingRecipesByOfferingRecipe(recipeId);
        } catch (Exception ignored) {
            //False exception when there is nothing to delete
        }

        try {
            offeringRecipeService.deleteOfferingRecipe(offeringRecipe);
        } catch (Exception ignored) {
            //False exception when there is nothing to delete
        }

        return true;
    }

}
