package ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.util.List;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class ClientOfferingRecipe {
    private String name;
    private List<String> selectedProducts;
    private String description;
    private String login;
    private String password;

}
