package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductsSetRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
public class ProductsSetService {

    @Autowired
    private ProductsSetRepository productsSetRepository;

    @Transactional
    public List<ProductsSet> getProductsSetsByUser(String login, String password) {
        return productsSetRepository.getProductsSetsByRecipeAppUser_LoginAndRecipeAppUser_Password(login, password);
    }

    @Transactional
    public ProductsSet getProductsSetByIdAndLoginAndPassword(BigDecimal id, String login, String password) {
        return productsSetRepository.getProductsSetByIdAndRecipeAppUser_LoginAndRecipeAppUser_Password(id, login, password);
    }

    @Transactional
    public void deleteProductsSetById(BigDecimal id) {
        productsSetRepository.deleteById(id);
    }

    @Transactional
    public ProductsSet createProductsSet(RecipeAppUser recipeAppUser, String name) {
        ProductsSet productsSet = new ProductsSet();
        productsSet.setRecipeAppUser(recipeAppUser);
        productsSet.setName(name);

        return productsSetRepository.saveAndFlush(productsSet);
    }
}
