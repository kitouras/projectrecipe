package ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.util.List;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class ProductsSetForCreate {
    private String selectedName;
    private List<String> selectedProducts;
    private String login;
    private String password;
}
