package ru.nsu.ccfit.vershinin.projectrecipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.nsu.ccfit.vershinin.projectrecipe.entities")
@ComponentScan("ru.nsu.ccfit.vershinin.projectrecipe.controllers")
@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
public class ProjectrecipeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectrecipeApplication.class, args);
    }

}
