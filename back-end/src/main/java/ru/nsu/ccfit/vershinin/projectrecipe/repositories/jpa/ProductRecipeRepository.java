package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductRecipe;

@Repository
public interface ProductRecipeRepository extends JpaRepository<ProductRecipe, ProductRecipe.ProductRecipeId> {
}
