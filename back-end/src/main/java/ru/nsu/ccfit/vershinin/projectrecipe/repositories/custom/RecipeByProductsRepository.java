package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom;

import java.util.List;

public interface RecipeByProductsRepository<T> {

    List<T> getRecipesByProducts(List<String> products);
}
