package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.OfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductOfferingRecipe;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ProductOfferingRecipeRepository extends JpaRepository<ProductOfferingRecipe, ProductOfferingRecipe.ProductOfferingRecipeId> {

    List<ProductOfferingRecipe> getProductOfferingRecipesByProductOfferingRecipeId_OfferingRecipe(OfferingRecipe offeringRecipe);

    void deleteByProductOfferingRecipeId_OfferingRecipe_Id(BigDecimal id);
}
