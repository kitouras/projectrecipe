package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "Recipe")
public class Recipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, precision = 10)
    private BigDecimal id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", nullable = false, length = 20000)
    private String description;

    @OneToMany(mappedBy = "productRecipeId.recipe", cascade = CascadeType.REMOVE)
    private final Set<ProductRecipe> productRecipeSet = new HashSet<>();
}
