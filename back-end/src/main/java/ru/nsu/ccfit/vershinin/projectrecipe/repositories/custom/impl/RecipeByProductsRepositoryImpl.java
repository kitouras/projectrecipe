package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Recipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.RecipeByProductsRepository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

public class RecipeByProductsRepositoryImpl implements RecipeByProductsRepository<Recipe> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL = "SELECT pr1.recipe id, r.description, r.name " +
            "FROM ((product_recipe pr1 INNER JOIN (SELECT p.id FROM product p WHERE p.name IN (:productsNames) ) p ON pr1.product = p.id) " +
            "INNER JOIN recipe r on r.id = pr1.recipe) " +
            "GROUP BY pr1.recipe, r.description, r.name  " +
            "HAVING count(pr1.recipe) >= ( " +
            "    SELECT count(pr2.recipe) " +
            "    FROM product_recipe pr2 " +
            "    WHERE pr2.recipe = pr1.recipe " +
            "    GROUP BY pr2.recipe " +
            "    )";

    @Override
    public List<Recipe> getRecipesByProducts(List<String> productsNames) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("productsNames", productsNames);

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(Objects.requireNonNull(jdbcTemplate.getDataSource()));

        return template.query(SQL, parameters, (rs, rowNum) -> {
            Recipe recipe = new Recipe();
            recipe.setId(rs.getBigDecimal(1));
            recipe.setDescription(rs.getString(2));
            recipe.setName(rs.getString(3));
            return recipe;
        });
    }

}
