package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.ProductFromSetRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class ProductFromSetService {

    @Autowired
    private ProductFromSetRepository productFromSetRepository;

    @Transactional
    public List<Product> getProductsByProductsSet(BigDecimal productsSetId) {
        return Objects.requireNonNullElse(productFromSetRepository.getProductsByProductsSet(productsSetId), Collections.emptyList());
    }

    @Transactional
    public void deleteByProductFromSetIdProductsSet(ProductsSet productsSet) {
        productFromSetRepository.deleteByProductFromSetIdProductsSet(productsSet);
    }

    @Transactional
    public void deleteProductFromSet(ProductFromSet productFromSet) {
        productFromSetRepository.delete(productFromSet);
    }

    @Transactional
    public ProductFromSet createProductFromSet(ProductFromSet productFromSet) {
        return productFromSetRepository.saveAndFlush(productFromSet);
    }
}
