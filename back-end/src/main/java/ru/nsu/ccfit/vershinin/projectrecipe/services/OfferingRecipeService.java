package ru.nsu.ccfit.vershinin.projectrecipe.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.NumberOfRecipesByModerators;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.OfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa.OfferingRecipeRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class OfferingRecipeService {
    @Autowired
    private OfferingRecipeRepository offeringRecipeRepository;

    @Transactional
    public List<NumberOfRecipesByModerators> getNumberOfRecipesByModerators() {
        return offeringRecipeRepository.getNumberOfRecipesByModerators();
    }

    @Transactional
    public List<OfferingRecipe> getOfferingRecipesByUser(String login, String password) {
        return offeringRecipeRepository.getOfferingRecipesByRecipeAppUser_LoginAndRecipeAppUser_Password(login, password);
    }

    @Transactional
    public Optional<OfferingRecipe> getOfferingRecipeById(BigDecimal id) {
        return offeringRecipeRepository.findById(id);
    }

    @Transactional
    public OfferingRecipe saveOfferingRecipe(OfferingRecipe offeringRecipe) {
        return offeringRecipeRepository.saveAndFlush(offeringRecipe);
    }

    @Transactional
    public void deleteOfferingRecipe(OfferingRecipe offeringRecipe) {
        offeringRecipeRepository.delete(offeringRecipe);
    }
}
