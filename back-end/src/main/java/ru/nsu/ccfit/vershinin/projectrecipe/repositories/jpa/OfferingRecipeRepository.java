package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.NumberOfRecipesByModerators;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.OfferingRecipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.NumberOfRecipesByModeratorsRepository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface OfferingRecipeRepository extends JpaRepository<OfferingRecipe, BigDecimal>, NumberOfRecipesByModeratorsRepository<NumberOfRecipesByModerators> {
    List<OfferingRecipe> getOfferingRecipesByRecipeAppUser_LoginAndRecipeAppUser_Password(String login, String password);
}
