package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductsSetForCreate;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.ProductsSetResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductFromSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductsSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeAppUserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class ProductsSetController {

    private final RecipeAppUserService recipeAppUserService;
    private final ProductsSetService productsSetService;
    private final ProductFromSetService productFromSetService;
    private final ProductService productService;

    public ProductsSetController(RecipeAppUserService recipeAppUserService, ProductsSetService productsSetService, ProductFromSetService productFromSetService, ProductService productService) {
        this.recipeAppUserService = recipeAppUserService;
        this.productsSetService = productsSetService;
        this.productFromSetService = productFromSetService;
        this.productService = productService;
    }

    @GetMapping("/look_sets")
    public List<ProductsSetResponse> getProductsSets(@RequestParam(name = "login") String login, @RequestParam(name = "password") String password) {
        List<ProductsSet> productsSets = productsSetService.getProductsSetsByUser(login, password);
        if (productsSets == null) {
            return Collections.emptyList();
        } else {
            List<ProductsSetResponse> productsSetResponseList = new ArrayList<>();
            for (ProductsSet productSet : productsSets) {
                ProductsSetResponse productsSetResponse = new ProductsSetResponse();
                productsSetResponse.setProducts(productFromSetService.getProductsByProductsSet(productSet.getId()).stream().map(pr -> ProductResponse.create(pr.getId(), pr.getName())).collect(Collectors.toList()));
                productsSetResponse.setId(productSet.getId());
                productsSetResponse.setName(productSet.getName());
                productsSetResponseList.add(productsSetResponse);
            }
            return productsSetResponseList;
        }
    }

    @DeleteMapping("/look_sets/{setIdStr}")
    public boolean deleteProductSet(@PathVariable String setIdStr, @RequestBody LoginPassword loginPassword) {
        BigDecimal setId = new BigDecimal(setIdStr);
        ProductsSet productsSetById = productsSetService.getProductsSetByIdAndLoginAndPassword(setId, loginPassword.getLogin(), loginPassword.getPassword());
        if (productsSetById != null) {
            try {
                productFromSetService.deleteByProductFromSetIdProductsSet(productsSetById);
            } catch (Exception ignored) {
                //False exception when there is nothing to delete
            }

            try {
                productsSetService.deleteProductsSetById(setId);
            } catch (Exception ignored) {
                //False exception when there is nothing to delete
            }
        }

        return true;
    }

    @PostMapping("/look_sets/")
    public ProductsSetResponse addProductsSets(@RequestBody ProductsSetForCreate productsSetForCreate) {
        List<String> productsNames = productsSetForCreate.getSelectedProducts();
        String selectedName = productsSetForCreate.getSelectedName();

        ProductsSetResponse productsSetResponse = new ProductsSetResponse();
        List<ProductResponse> products = new ArrayList<>();

        RecipeAppUser recipeAppUserByLogin = recipeAppUserService.getUserByNameAndPassword(productsSetForCreate.getLogin(),
                productsSetForCreate.getPassword());
        if (recipeAppUserByLogin != null) {
            ProductsSet productsSet = productsSetService.createProductsSet(recipeAppUserByLogin, selectedName);

            productsSetResponse.setName(productsSet.getName());
            productsSetResponse.setId(productsSet.getId());

            for (String productsName : productsNames) {
                Product productByName = productService.getProductByName(productsName);
                ProductFromSet productFromSet = productFromSetService
                        .createProductFromSet(ProductFromSet
                                .create(ProductFromSet.ProductFromSetId.create(productsSet, productByName)));
                products.add(ProductResponse.create(productFromSet.getProductFromSetId().getProduct().getId(),
                        productFromSet.getProductFromSetId().getProduct().getName()));
            }
            productsSetResponse.setProducts(products);
        }

        return productsSetResponse;

    }
}
