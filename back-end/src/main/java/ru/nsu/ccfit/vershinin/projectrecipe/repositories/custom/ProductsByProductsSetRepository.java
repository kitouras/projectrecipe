package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom;

import java.math.BigDecimal;
import java.util.List;

public interface ProductsByProductsSetRepository<T> {

    List<T> getProductsByProductsSet(BigDecimal productsSetId);
}
