package ru.nsu.ccfit.vershinin.projectrecipe.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class NumberOfRecipesByModerators {
    private BigDecimal moderatorId;
    private BigDecimal countRecipes;
}
