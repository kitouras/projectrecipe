package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "ProductsSet")
public class ProductsSet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, precision = 10)
    private BigDecimal id;
    @ManyToOne
    @JoinColumn(name = "recipeAppUser")
    private RecipeAppUser recipeAppUser;
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "productFromSetId.productsSet", cascade = CascadeType.REMOVE)
    private final Set<ProductFromSet> productFromSetSet = new HashSet<>();
}
