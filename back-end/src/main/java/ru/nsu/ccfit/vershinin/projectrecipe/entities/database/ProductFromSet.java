package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "ProductFromSet")
public class ProductFromSet implements Serializable {
    @EmbeddedId
    private ProductFromSetId productFromSetId;

    @Generated
    @AllArgsConstructor(staticName = "create")
    @NoArgsConstructor(force = true)
    @Data
    @Embeddable
    public static class ProductFromSetId implements Serializable {
        @ManyToOne
        @JoinColumn(name = "productsSet")
        private ProductsSet productsSet;
        @ManyToOne
        @JoinColumn(name = "product")
        private Product product;
    }
}
