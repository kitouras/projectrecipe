package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.NumberOfRecipesByModerators;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.NumberOfRecipesByModeratorsRepository;

import java.util.List;

public class NumberOfRecipesByModeratorsRepositoryImpl implements NumberOfRecipesByModeratorsRepository<NumberOfRecipesByModerators> {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL = "select R_A_U.ID moderatorId,  COALESCE(count(O_R.ID), 0) countRecipes\n" +
            "FROM (OFFERING_RECIPE O_R RIGHT JOIN RECIPE_APP_USER R_A_U ON O_R.RECIPE_APP_USER = R_A_U.ID) " +
            "WHERE R_A_U.ROLE = 'M'" +
            "GROUP BY R_A_U.ID";

    @Override
    public List<NumberOfRecipesByModerators> getNumberOfRecipesByModerators() {
        return jdbcTemplate.query(SQL, (rs, rowNum) -> {
            NumberOfRecipesByModerators numberOfRecipesByModerators = new NumberOfRecipesByModerators();
            numberOfRecipesByModerators.setModeratorId(rs.getBigDecimal("moderatorId"));
            numberOfRecipesByModerators.setCountRecipes(rs.getBigDecimal("countRecipes"));
            return numberOfRecipesByModerators;
        });
    }
}
