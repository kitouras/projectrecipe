package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Recipe;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.RecipeByProductsRepository;

import java.math.BigDecimal;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, BigDecimal>, RecipeByProductsRepository<Recipe> {

}
