package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Recipe;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeService;

import java.util.List;

@RestController
@RequestMapping("/get_recipes")
public class RecipesByProductsController {

    private final RecipeService recipeService;
    private final ProductService productService;

    public RecipesByProductsController(RecipeService recipeService, ProductService productService) {
        this.recipeService = recipeService;
        this.productService = productService;
    }

    @GetMapping(path = "")
    public List<String> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping(path = "/search")
    public List<Recipe> getRecipesByProducts(@RequestParam(name = "products") List<String> products) {
        return recipeService.getRecipesByProducts(products);
    }
}
