package ru.nsu.ccfit.vershinin.projectrecipe.entities.responses;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductResponse;

import java.math.BigDecimal;
import java.util.List;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class ProductsSetResponse {
    private BigDecimal id;
    private String name;
    private List<ProductResponse> products;
}
