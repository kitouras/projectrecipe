package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "ProductRecipe")
public class ProductRecipe implements Serializable {

    @EmbeddedId
    private ProductRecipeId productRecipeId;

    @Generated
    @AllArgsConstructor(staticName = "create")
    @NoArgsConstructor(force = true)
    @Embeddable
    @Data
    public static class ProductRecipeId implements Serializable {
        @ManyToOne
        @JoinColumn(name = "product")
        private Product product;
        @ManyToOne
        @JoinColumn(name = "recipe")
        private Recipe recipe;
    }
}
