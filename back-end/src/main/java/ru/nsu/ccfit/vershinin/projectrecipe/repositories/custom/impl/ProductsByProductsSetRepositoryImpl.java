package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom.ProductsByProductsSetRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class ProductsByProductsSetRepositoryImpl implements ProductsByProductsSetRepository<Product> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String SQL = "SELECT P.ID, P.NAME " +
            "FROM (PRODUCT_FROM_SET PFS INNER JOIN PRODUCT P on PFS.PRODUCT = P.ID) " +
            "WHERE PFS.PRODUCTS_SET = :productsSetId";

    @Override
    public List<Product> getProductsByProductsSet(BigDecimal productsSetId) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("productsSetId", productsSetId);

        NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(Objects.requireNonNull(jdbcTemplate.getDataSource()));

        return template.query(SQL, parameters, (rs, rowNum) -> {
            Product product = new Product();
            product.setId(rs.getBigDecimal("ID"));
            product.setName(rs.getString("NAME"));
            return product;
        });

    }
}
