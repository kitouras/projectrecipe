package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.LoginResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.services.RecipeAppUserService;

@RestController
@RequestMapping("/")
public class UserController {

    private final RecipeAppUserService recipeAppUserService;

    public UserController(RecipeAppUserService recipeAppUserService) {
        this.recipeAppUserService = recipeAppUserService;
    }

    @GetMapping("/login")
    public LoginResponse logIn(@RequestParam(name = "login") String login,
                               @RequestParam(name = "password") String password) {
        RecipeAppUser userByNameAndPassword = recipeAppUserService.getUserByNameAndPassword(login, password);
        LoginResponse loginResponse;
        if (userByNameAndPassword == null) {
            loginResponse = LoginResponse.create("", "", "", "N");
        } else {
            loginResponse = LoginResponse.create(userByNameAndPassword.getLogin(), userByNameAndPassword.getPassword(), userByNameAndPassword.getRole(), "Y");
        }
        return loginResponse;
    }

    @PostMapping("/register")
    public boolean register(@RequestBody LoginPassword loginPassword) {
        String login = loginPassword.getLogin();
        String password = loginPassword.getPassword();

        RecipeAppUser recipeAppUserByLogin = recipeAppUserService.getRecipeAppUserByLogin(login);
        if (recipeAppUserByLogin == null) {
            RecipeAppUser newRecipeAppUser = new RecipeAppUser();
            newRecipeAppUser.setLogin(login);
            newRecipeAppUser.setPassword(password);
            newRecipeAppUser.setRole("U");
            RecipeAppUser savedUSer = recipeAppUserService.saveUser(newRecipeAppUser);
            return savedUSer != null && savedUSer.getId() != null;
        } else {
            return false;
        }
    }

}
