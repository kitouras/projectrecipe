package ru.nsu.ccfit.vershinin.projectrecipe.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.LoginPassword;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities.ProductsForAdding;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.Product;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductFromSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.ProductsSet;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.responses.ProductsSetResponse;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductFromSetService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductService;
import ru.nsu.ccfit.vershinin.projectrecipe.services.ProductsSetService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class ProductFromSetController {

    private final ProductFromSetService productFromSetService;
    private final ProductService productService;
    private final ProductsSetService productsSetService;

    public ProductFromSetController(ProductFromSetService productFromSetService, ProductService productService, ProductsSetService productsSetService) {
        this.productFromSetService = productFromSetService;
        this.productService = productService;
        this.productsSetService = productsSetService;
    }

    @PostMapping("/look_sets/products")
    public ProductsSetResponse addProductsIntoSet(@RequestBody ProductsForAdding productsForAdding) {
        ProductsSetResponse productsSetResponse = new ProductsSetResponse();
        BigDecimal setId = productsForAdding.getSetId();
        List<String> products = productsForAdding.getProducts();
        ProductsSet productsSetById = productsSetService.getProductsSetByIdAndLoginAndPassword(setId, productsForAdding.getLogin(), productsForAdding.getPassword());

        if (productsSetById != null) {
            for (String productName : products) {
                Product productByName = productService.getProductByName(productName);
                productFromSetService.createProductFromSet(ProductFromSet.create(ProductFromSet.ProductFromSetId.create(productsSetById, productByName)));
            }

            productsSetResponse.setId(setId);
            productsSetResponse.setName(productsSetById.getName());
            productsSetResponse.setProducts(productFromSetService.getProductsByProductsSet(setId).stream().map(pr -> ProductResponse.create(pr.getId(), pr.getName())).collect(Collectors.toList()));

        }

        return productsSetResponse;
    }

    @DeleteMapping("/look_sets/{setId}/product/{productId}")
    public ProductsSetResponse deleteProductFromSet(@PathVariable String setId,
                                                    @PathVariable String productId,
                                                    @RequestBody LoginPassword loginPassword) {
        ProductsSetResponse productsSetResponse = new ProductsSetResponse();
        BigDecimal setIdNum = new BigDecimal(setId);
        BigDecimal productIdNum = new BigDecimal(productId);

        ProductsSet productsSetById = productsSetService.getProductsSetByIdAndLoginAndPassword(setIdNum, loginPassword.getLogin(), loginPassword.getPassword());

        if (productsSetById != null) {
            Optional<Product> productById = productService.getProductById(productIdNum);
            if (productById.isPresent()) {
                productFromSetService.deleteProductFromSet(ProductFromSet.create(ProductFromSet.ProductFromSetId.create(productsSetById, productById.get())));

                productsSetResponse.setId(setIdNum);
                productsSetResponse.setName(productsSetById.getName());
                productsSetResponse.setProducts(productFromSetService.getProductsByProductsSet(setIdNum).stream().map(pr -> ProductResponse.create(pr.getId(), pr.getName())).collect(Collectors.toList()));
            }
        }

        return productsSetResponse;
    }
}
