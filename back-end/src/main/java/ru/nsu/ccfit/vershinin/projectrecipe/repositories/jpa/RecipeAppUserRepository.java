package ru.nsu.ccfit.vershinin.projectrecipe.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.ccfit.vershinin.projectrecipe.entities.database.RecipeAppUser;

import java.math.BigDecimal;

@Repository
public interface RecipeAppUserRepository extends JpaRepository<RecipeAppUser, BigDecimal> {

    RecipeAppUser findRecipeAppUserByLogin(String login);

    RecipeAppUser getRecipeAppUserByLoginAndPassword(String login, String password);

}
