package ru.nsu.ccfit.vershinin.projectrecipe.entities.database;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
@Entity
@Table(name = "Product")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, precision = 10)
    private BigDecimal id;
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "productRecipeId.product", cascade = CascadeType.REMOVE)
    private final Set<ProductRecipe> productRecipeSet = new HashSet<>();

    @OneToMany(mappedBy = "productFromSetId.product", cascade = CascadeType.REMOVE)
    private final Set<ProductFromSet> productFromSetSet = new HashSet<>();

    @OneToMany(mappedBy = "productOfferingRecipeId.product", cascade = CascadeType.REMOVE)
    private final Set<ProductOfferingRecipe> productOfferingRecipes = new HashSet<>();
}
