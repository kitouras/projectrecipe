package ru.nsu.ccfit.vershinin.projectrecipe.entities.client_entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Generated
@AllArgsConstructor(staticName = "create")
@NoArgsConstructor(force = true)
@Data
public class ProductResponse {
    private BigDecimal id;
    private String name;

}
