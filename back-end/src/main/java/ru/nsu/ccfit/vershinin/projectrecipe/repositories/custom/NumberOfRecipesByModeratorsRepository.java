package ru.nsu.ccfit.vershinin.projectrecipe.repositories.custom;

import java.util.List;

public interface NumberOfRecipesByModeratorsRepository<T> {

    List<T> getNumberOfRecipesByModerators();

}
