create or replace TRIGGER auto_id_route
    before insert ON PRODUCT
    FOR EACH ROW
declare
    cursor get_route_ids is
        select ID
        from PRODUCT
        order by ID;

    prev_id PRODUCT.ID%type := 0;
begin
    if (:new.ID is null) then
        for curr_id in get_route_ids loop
                if ((curr_id.ID - prev_id) > 1) THEN
                    exit;
                end if;
                prev_id := curr_id.ID;
            end loop;
        :new.ID := prev_id + 1;

    end if;
end;