import React from "react"
import ListItem from "@material-ui/core/ListItem";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import Collapse from "@material-ui/core/Collapse";
import AddProductDialog from "./AddProductDialogComponent";
import List from "@material-ui/core/List";

export default function setsProductsSet(props) {

    const {set,
        isOpenFlag,
        isOpenAddProductDialogFlag,
        products,
        handleOpenSet,
        handleOpenAddProductDialog,
        handleDeleteSet,
        handleAddProducts,
        handleCloseAddProductDialog,
        handleDeleteProduct} = props

    function getProductsNotFromSet(products) {
        const productsNotFromSet = products.slice()
        const setProducts = set.products;
        for (let i = 0; i < setProducts.length; ++i) {
            let indArr = productsNotFromSet.indexOf(setProducts[i].name)
            if (indArr > -1) {
                productsNotFromSet.splice(indArr, 1)
            }
        }

        return productsNotFromSet
    }

    return (
        <div>
            <ListItem button
                      key={"set=".concat(set.id)}
                      onClick={() => handleOpenSet(set.id)}
                      shouldComponentUpdate>
                {isOpenFlag ? <ExpandLess /> : <ExpandMore />}
                <ListItemText primary={set.name} style={{fontStyle: "italic"}}/>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="add" onClick={() => handleOpenAddProductDialog(set.id)}>
                        <AddIcon />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteSet(set)}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            <AddProductDialog products={getProductsNotFromSet(products)} set={set} isOpen={isOpenAddProductDialogFlag} handleClose={handleCloseAddProductDialog} handleAddSet={handleAddProducts} />
            <Collapse in={isOpenFlag} timeout={"auto"} unmountOnExit addEndListener={() => {}}>
                <List component="div" disablePadding>
                    {set.products.map((product) => {
                        return (
                            <ListItem key={'set='.concat(set.id).concat(',recipe=').concat(product.id)}>
                                <ListItemText id={product.id} primary={product.name} />
                                <ListItemSecondaryAction>
                                    <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteProduct(product.id, set)}>
                                        <DeleteIcon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            </ListItem>
                        )
                    })}
                </List>
            </Collapse>
        </div>
    )
}