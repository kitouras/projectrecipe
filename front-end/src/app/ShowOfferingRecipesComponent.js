import React from "react"
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from "@material-ui/icons/Delete";
import ListItem from "@material-ui/core/ListItem";
import Collapse from "@material-ui/core/Collapse";
import Typography from "@material-ui/core/Typography";

export default function ShowOfferingRecipes(props) {

    const { login, password, offeringRecipes, setOfferingRecipes } = props
    const [localOfferingRecipes, setLocalOfferingRecipes] = React.useState(initLocalOfferingRecipes())

    function initLocalOfferingRecipes() {
        return {offeringRecipes: offeringRecipes,
                isOpenFlags: initFlags()
        }
    }
    function initFlags() {
        let bufMap = new Map()
        let len = offeringRecipes.length
        for (let i = 0; i < len; ++i) {
            bufMap.set(offeringRecipes[i].id, false);
        }
        return bufMap
    }

    const handleOpenRecipe = (recipeId) => {
        let newFlag = !localOfferingRecipes.isOpenFlags.get(recipeId)
        setLocalOfferingRecipes({offeringRecipes: offeringRecipes,
            isOpenFlags: localOfferingRecipes.isOpenFlags.set(recipeId, newFlag)
        })
    }

    const handleApproveRecipe = (recipe) => {
        let recipeId = recipe.id
        approveNewRecipeQuery(recipeId).then(r => {
            if (r === true) {
                deleteRecipeFromList(recipe)
            }
        })
    }

    const handleRejectRecipe = (recipe) => {
        let recipeId = recipe.id
        rejectRecipeQuery(recipeId).then(r => {
            if (r === true) {
                deleteRecipeFromList(recipe)
            }
        })
    }

    function approveNewRecipeQuery(recipeId) {
        return fetch("/show_offering_recipes/approve/"+recipeId, {
            method: "POST",
            headers: {'Content-Type' : 'application/json'},
            body: JSON.stringify({
                login, password
            })
        }).then(response => {
            return response.json()
        })
    }

    function rejectRecipeQuery(recipeId) {
        return fetch("/show_offering_recipes/reject/"+recipeId, {
            method: "DELETE",
            headers: {'Content-Type' : 'application/json'},
            body: JSON.stringify({
                login, password
            })
        }).then(response => {
            return response.json()
        })
    }

    function deleteRecipeFromList(recipe) {
        let recipeId = recipe
        const indexEl = offeringRecipes.indexOf(recipe)
        if (indexEl > -1) {
            offeringRecipes.splice(indexEl, 1)
        }
        setOfferingRecipes(offeringRecipes)

        localOfferingRecipes.isOpenFlags.delete(recipeId)

        setLocalOfferingRecipes({offeringRecipes: offeringRecipes,
            isOpenFlags: localOfferingRecipes.isOpenFlags
        })
    }

    return (
        <List component="nav"
              aria-labelledby="nested-offering-recipes-subheader"
              subheader={
                  <ListSubheader component="div" id="nested-offering-recipes-subheader">
                      Предложенные рецепты для рассмотрения
                  </ListSubheader>
              }
        >
            <div style={{padding: "30px"}}/>
            {localOfferingRecipes.offeringRecipes.map((localOfferingRecipe) => {
                const isOpenFlag = localOfferingRecipes.isOpenFlags.get(localOfferingRecipe.id)
                const recipeId = localOfferingRecipe.id
                return (
                    <div>
                        <ListItem button
                                  key={"set=".concat(recipeId)}
                                  onClick={() => handleOpenRecipe(recipeId)}
                                  shouldComponentUpdate>
                            {isOpenFlag ? <ExpandLess /> : <ExpandMore />}
                            <ListItemText primary={localOfferingRecipe.name} style={{fontStyle: "italic"}}/>
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="add" onClick={() => handleRejectRecipe(localOfferingRecipe)}>
                                    <DeleteIcon />
                                </IconButton>
                                <IconButton edge="end" aria-label="add" onClick={() => handleApproveRecipe(localOfferingRecipe)}>
                                    <SaveIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Collapse in={isOpenFlag} timeout={"auto"} unmountOnExit addEndListener={() => {}}>
                            <List component="div" disablePadding>
                                {localOfferingRecipe.selectedProducts.map((product) => {
                                    return (
                                        <ListItem key={'recipe='.concat(recipeId).concat(',product=').concat(product)}>
                                            <ListItemText id={product} primary={product} />
                                        </ListItem>
                                    )
                                })}
                            </List>
                            <div style={{padding: "5px"}}/>
                            <Typography>
                                {localOfferingRecipe.description}
                            </Typography>
                        </Collapse>
                    </div>
                )
            })}
        </List>
    )
}