import React from "react"
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom"
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {makeStyles} from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Login(props) {

    const history = useHistory();
    const classes = useStyles();

    const { setState, setUserName, setPassword, setRole, setSets, setOfferingRecipes } = props

    const [isSuccess, setSuccess] = React.useState(true);
    const [localName, setLocalName] = React.useState("")
    const [localPassword, setLocalPassword] = React.useState("")

    const handleLoginEnter = () => {
        getQuery("/login?login=".concat(localName).concat("&password=").concat(localPassword)).then(r => {
            if (r.state === "Y") {
                setState("Y")
                setUserName(r.login)
                setPassword(r.password)
                setRole(r.role)

                getQuery("/look_sets?login=".concat(r.login).concat("&password=").concat(r.password)).then(rSets => {
                    setSets(rSets)
                })
                if (r.role === "M") {
                    getQuery("/show_offering_recipes?login=".concat(r.login).concat("&password=").concat(r.password)).then(rOfferingRecipes => {
                        setOfferingRecipes(rOfferingRecipes)
                    })
                }

                setSuccess(true)
                history.push("/")
            } else {
                setSuccess(false)
            }
        })
    }

    function getQuery(url) {
        return fetch(url, {
            method: "GET"
        }).then(response => {
            return response.json()
        });
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Вход
                </Typography>
                    <TextField
                        inputProps={{ "data-testid": "loginField" }}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="login"
                        label="Логин"
                        name="login"
                        autoComplete="login"
                        autoFocus
                        onChange={(e) => setLocalName(e.target.value)}
                    />
                    <TextField
                        inputProps={{ "data-testid": "passwordField" }}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Пароль"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={(e) => setLocalPassword(e.target.value)}
                    />
                    <Button
                        data-testid="login"
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleLoginEnter}
                    >
                        Войти
                    </Button>
                    <Link to="/register">
                        {"Ещё не зарегистрированы? Зарегистрируетесь!"}
                    </Link>
                <div style={{padding: "10px"}}/>
                {isSuccess
                    ?
                        (<div/>)
                    : <Alert severity="error">
                        <AlertTitle>Ошибка!</AlertTitle>
                        <strong>Неверный логин или пароль!</strong>
                    </Alert>
                }
            </div>
        </Container>
    )
}