import React from "react"
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";

const useStyles = makeStyles((theme) => ({
    root: {
        width: "80%",
        '& > * + *': {
            marginTop: theme.spacing(3),
        },
    },
}));

const backgroundStyle = {
    width: "80%",
    height: "80%",
    backgroundSize: 'cover',
    position: 'absolute', left: '60%', top: '55%',
    transform: 'translate(-50%, -50%)'

}

const recipeStyle = makeStyles((theme) => ({
    root : {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular
    }
}))

export default function GetRecipes(props) {

    const [recipes, setRecipes] = React.useState([])
    const [isOpenSet, setOpenSet] = React.useState(false)
    const [selectedSet, setSelectedSet] = React.useState(null)
    const [selectedProducts, setSelectedProducts] = React.useState(null)

    const recipeClasses = recipeStyle()
    const classes = useStyles();

    function getRecipesByProducts(localSelectedProducts) {
        return fetch("get_recipes/search?products=".concat(localSelectedProducts.join(","))).then(responce => {
            return responce.json()
        })
    }

    const handleSetOpenSet = () => {
        if (selectedSet === null) {
            setOpenSet(false)
        } else {
            setOpenSet(!isOpenSet)
        }
    }

    const handleRequestRecipes = () => {
        let localSelectedProducts
        if (selectedSet !== null) {
            localSelectedProducts = selectedSet.products.map(product => product.name)
        } else {
            localSelectedProducts = selectedProducts;
        }
        if (localSelectedProducts !== undefined && localSelectedProducts !== null && localSelectedProducts.length !== null && localSelectedProducts.length > 0) {
            getRecipesByProducts(localSelectedProducts).then(r => {
                console.log(r)
                setRecipes(r)
            });
        }
    };

    return (
        <div style={backgroundStyle}>
                <div className={classes.root}>
                    <Grid container spacing={2}>
                        <Grid item xs>
                            <Autocomplete
                                multiple
                                id="products"
                                options={props.products}
                                getOptionLabel={(option) => option}
                                onChange={(e, data) => {
                                    setSelectedProducts(data)
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        label="Продукты"
                                        placeholder="Продукт"
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs>
                            <Autocomplete
                                id="productsSets"
                                options={props.sets}
                                getOptionLabel={option => option.name}
                                renderOption={(option) => (
                                    <React.Fragment>
                                        <span>{option.id}:  </span>
                                        {option.name}
                                    </React.Fragment>
                                )}
                                onChange={(e, selSet) => setSelectedSet(selSet)}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant="standard"
                                        label="Наборы продуктов"
                                        placeholder={"Наборы продуктов"}
                                        inputProps={{
                                            ...params.inputProps,
                                        }}
                                    />
                                )}

                            />
                            <div style={{padding: "5px"}}/>
                            <Button variant="contained"
                                    color="primary"
                                    onClick={handleSetOpenSet}>
                                Посмотреть продукты из набора
                            </Button>
                            <Collapse in={isOpenSet} timeout={"auto"} unmountOnExit addEndListener={() => {}}>
                                <List component="div" disablePadding>
                                    {selectedSet !== null ?
                                        selectedSet.products.map((product) => {
                                        return (
                                            <ListItem key={'set='.concat(selectedSet.id).concat(',recipe=').concat(product.id)}>
                                                <ListItemText id={product.id} primary={product.name} />
                                            </ListItem>
                                        )
                                    })
                                        :
                                    <div/>}
                                </List>
                            </Collapse>
                        </Grid>

                    </Grid>
                    <div style={{padding: "2px"}}/>
                    <Button type="submit"
                            variant="contained"
                            color="primary"
                            onClick={handleRequestRecipes}
                    >
                        Запросить
                    </Button>
                    <div style={{padding: "10px"}}/>
                    <List component="nav" aria-label="main mailbox folders">
                        {recipes.map((recipe) => {
                            return (
                                <ExpansionPanel>
                                    <ExpansionPanelSummary
                                        aria-controls="panel1a-content"
                                        id="get_recipes+header"
                                    >
                                        <Typography className={recipeClasses.root} style={{fontStyle: "italic"}}>{recipe.name}</Typography>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails>
                                        <Typography>
                                            {recipe.description}
                                        </Typography>
                                    </ExpansionPanelDetails>
                                    <div style={{padding: "2px"}}/>
                                </ExpansionPanel>
                            )
                        })}
                    </List>
                </div>
        </div>
    )

}