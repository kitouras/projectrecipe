import React, {useState} from "react"
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import DialogContent from "@material-ui/core/DialogContent";

export default function CreateSetDialog(props) {

    const { products, isOpen, handleClose, handleCreateSet } = props

    const [selectedName, setSelectedName] = useState("")
    const [selectedProducts, setSelectedProducts] = useState([])
    const [isEmptyName, setEmptyName] = useState(false)

    const handleCreateSetWithCheck = () => {
        if (selectedName === "") {
            setEmptyName(true)
        } else {
            setEmptyName(false)
            handleCreateSet(selectedName, selectedProducts)
        }
    }

    return (
        <Dialog onClose={handleClose} open={isOpen} aria-labelledby="create-set-title">
            <DialogTitle id="create-set-title">
                Придумайте имя для набора и выберите продукты.
            </DialogTitle>
            <DialogContent dividers>
                <TextField
                    inputProps={{ "data-testid": "name" }}
                    error={isEmptyName}
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    label="Имя набора"
                    id="name"
                    onChange={(e) => {
                        setSelectedName(e.target.value)
                    }}
                />
                <Autocomplete
                    multiple
                    id="products"
                    options={products}
                    getOptionLabel={(option) => option}
                    onChange={(e, data) => {
                        setSelectedProducts(data);
                    }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            label="Продукты"
                            placeholder="Продукт"
                        />
                    )}
                />
                <div style={{padding: "5px"}}/>
                <Grid container spacing={2}>
                    <Grid item xs>
                        <Button
                            data-testid={"cancel"}
                            variant={"contained"}
                            onClick={handleClose}>
                            Отмена
                        </Button>
                    </Grid>
                    <Grid item xs>
                        <Button
                            data-testid={"create"}
                            variant={"contained"}
                            onClick={handleCreateSetWithCheck}>
                            Создать набор
                        </Button>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
}