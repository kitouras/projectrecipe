import React from "react"
import List from "@material-ui/core/List";
import ListSubheader from "@material-ui/core/ListSubheader";
import Button from "@material-ui/core/Button";
import CreateSetDialog from "./CreateSetDialogComponent";
import ProductsSet from "./ProductsSetComponent";

export default function ProductSet(props) {

    let {sets, setSets, products, login, password} = props;

    const [localSetsData, setLocalSetsData] = React.useState(initLocalSetsData())
    const [isOpenCreateSetDialog, setOpenCreateSetDialog] = React.useState(false)

    function initLocalSetsData() {
        return {sets: sets,
            isOpenFlags: initFlags(),
            isOpenAddProductDialogFlags: initFlags()}
    }

    function initFlags() {
        let bufMap = new Map()
        let len = sets.length
        for (let i = 0; i < len; ++i) {
            bufMap.set(sets[i].id, false);
        }
        return bufMap
    }

    const handleDeleteSet = (set) => {
        let setId = set.id
        deleteSetRequest(setId).then((r) => {
            if (r === true) {
                const indexEl = sets.indexOf(set)
                if (indexEl > -1) {
                    sets.splice(indexEl, 1)
                }
                setSets(sets)

                localSetsData.isOpenFlags.delete(setId)
                localSetsData.isOpenAddProductDialogFlags.delete(setId)

                setLocalSetsData({sets: sets,
                    isOpenFlags: localSetsData.isOpenFlags,
                    isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags})
            }
        })
    }

    const handleOpenSet = (key) => {
        let newFlag = !localSetsData.isOpenFlags.get(key)
        setLocalSetsData({sets: sets,
            isOpenFlags: localSetsData.isOpenFlags.set(key, newFlag),
            isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags})
    }

    const handleCloseCreateSetDialog = () => {
        setOpenCreateSetDialog(false)
    }

    const handleOpenCreateSetDialog = () => {
        setOpenCreateSetDialog(true)
    }

    const handleCreateSet = (selectedName, selectedProducts) => {
        setOpenCreateSetDialog(false)
        createSetQuery(selectedName, selectedProducts).then(r => {
            addLocalSet(r)
        })
    }

    const handleCloseAddProductDialog = (setId) => {
        setLocalSetsData({sets: sets,
            isOpenFlags: localSetsData.isOpenFlags,
            isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags.set(setId, false)})
    }

    const handleOpenAddProductDialog = (setId) => {
        setLocalSetsData({sets: sets,
            isOpenFlags: localSetsData.isOpenFlags,
            isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags.set(setId, true)})
    }

    const handleAddProducts = (set, products) => {
        localSetsData.isOpenFlags.set(set.id, false)
        setLocalSetsData({sets: sets,
            isOpenFlags: localSetsData.isOpenFlags,
            isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags.set(set.id, false)})
        addProductsQuery(set.id, products).then(r => {
            updateSet(set, r)
        })
    }

    const handleDeleteProduct = (productId, set) => {
        deleteProductFromSetQuery(productId, set.id).then(r => {
            updateSet(set, r)
        })
    }

    function deleteSetRequest(setId) {
        return fetch("/look_sets/" + setId, {
            method: "DELETE",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                login, password
            })
        }).then(response => {
            return response.json()
        })
    }

    function addProductsQuery(setId, products) {
        return fetch("/look_sets/products/", {
            method: "POST",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                setId, products, login, password
            })
        }).then(response => {
            return response.json();
        })
    }

    function createSetQuery(selectedName, selectedProducts) {
        return fetch("/look_sets/", {
            method: "POST",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                selectedName, selectedProducts, login, password
            })
        }).then((response) => {
            return response.json()
        })
    }

    function deleteProductFromSetQuery(productId, setId) {
        return fetch("/look_sets/"+setId+"/product/"+productId, {
            method: "DELETE",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                login, password
            })
        }).then(response => {
            return response.json()
        })
    }

    const updateSet = (oldSet, newSet) => {
        if (newSet.id !== null) {
            let localProductsSets = sets.slice();
            const indexEl = localProductsSets.indexOf(oldSet)
            if (indexEl > -1) {
                localProductsSets[indexEl] = newSet
            }

            setSets(localProductsSets)

            setLocalSetsData({sets: localProductsSets, isOpenFlags: localSetsData.isOpenFlags, isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags})
        }
    }

    const addLocalSet = (newSet) => {
        setSets([...sets, newSet])

        setLocalSetsData({sets: [...sets, newSet],
            isOpenFlags: localSetsData.isOpenFlags.set(newSet.id, false),
            isOpenAddProductDialogFlags: localSetsData.isOpenAddProductDialogFlags.set(newSet.id, false)})
    }

    const renderSet = () => {
        return localSetsData.sets.map((set) => (
            <ProductsSet set={set}
                         isOpenFlag={localSetsData.isOpenFlags.get(set.id)}
                         isOpenAddProductDialogFlag={localSetsData.isOpenAddProductDialogFlags.get(set.id)}
                         products={products}
                         handleOpenSet={handleOpenSet}
                         handleOpenAddProductDialog={handleOpenAddProductDialog}
                         handleDeleteSet={handleDeleteSet}
                         handleAddProducts={handleAddProducts}
                         handleCloseAddProductDialog={handleCloseAddProductDialog}
                         handleDeleteProduct={handleDeleteProduct}
            />

        ))
    }

    return (
        <div>
            <List component="nav"
                  aria-labelledby="nested-sets-subheader"
                  subheader={
                      <ListSubheader component="div" id="nested-sets-subheader">
                          Наборы продуктов
                      </ListSubheader>
                  }
            >
                <div style={{padding: "10px"}}/>
                <Button variant={"contained"} color="primary" onClick={handleOpenCreateSetDialog} style={{position: 'absolute', left: '40%'}}>
                    Создайте новый набор продуктов
                </Button>
                <div style={{padding: "30px"}}/>
                <CreateSetDialog products={products} isOpen={isOpenCreateSetDialog} handleClose={handleCloseCreateSetDialog} handleCreateSet={handleCreateSet}/>
                {renderSet()}
            </List>
        </div>

    )
}