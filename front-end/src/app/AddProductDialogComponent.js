import React, {useState} from "react"
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

export default function AddProductDialog(props) {
    const { products, set, isOpen, handleClose, handleAddSet } = props
    const [selectedProducts, setSelectedProducts] = useState([])

    return (
        <Dialog onClose={() => {handleClose(set.id)}} open={isOpen} aria-labelledby="add-products-title">
            <DialogTitle id="add-products-title">
                Выберите продукты для добавления
            </DialogTitle>
            <DialogContent dividers>
                <Autocomplete
                    multiple
                    id="products"
                    options={products}
                    getOptionLabel={(option) => option}
                    onChange={(e, data) => {
                        setSelectedProducts(data);
                    }}
                    data-testid={"textFieldProducts"}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            label="Продукты"
                            placeholder="Продукт"
                        />
                    )}
                />
                <div style={{padding: "5px"}}/>
                <Grid container spacing={2}>
                    <Grid item xs>
                        <Button
                            data-testid="cancel"
                            variant={"contained"}
                            onClick={() => {
                                handleClose(set.id)
                        }}>
                            Отмена
                        </Button>
                    </Grid>
                    <Grid item xs>
                        <Button
                            data-testid="add"
                            variant={"contained"}
                            onClick={() => {
                                handleAddSet(set, selectedProducts)
                        }}>
                            Добавить продукты
                        </Button>
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
}