import React from "react";

const backgroundStyle = {
    width: "80%",
    height: "80%",
    backgroundImage: "url(".concat("home_background.jpg").concat(")"),
    backgroundSize: 'cover',
    position: 'absolute', left: '50%', top: '50%',
    transform: 'translate(-50%, -50%)'

}

export default function Home() {

    return (
        <div align={"center"} style={backgroundStyle}>
            <div style={{position: 'absolute', left: '50%', top: '50%',
                        transform: 'translate(-50%, -50%)',
                        color: "white",
                        size: "100px"}}>
                <h1>
                    Добро пожаловать в онлайн книгу рецептов!
                </h1>
            </div>
        </div>
    )
}