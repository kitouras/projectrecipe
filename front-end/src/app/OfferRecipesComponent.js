import React from "react"
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Button from "@material-ui/core/Button";
import AlertTitle from "@material-ui/lab/AlertTitle";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    header: {
        fontStyle: "italic"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function OfferRecipes(props) {

    const classes = useStyles()

    const { products, login, password } = props;

    const [name, setName] = React.useState("")
    const [selectedProducts, setSelectedProducts] = React.useState([])
    const [description, setDescription] = React.useState("")
    const [isSuccess, setSuccess] = React.useState(false)
    const [isFailure, setFailure] = React.useState(false)

    const handleOfferRecipe = () => {
        console.log("aaa")
        offerRecipeQuery().then((r) => {
            if (r === true) {
                setSuccess(true)
                setFailure(false)
            } else {
                setSuccess(false)
                setFailure(true)
            }
        })
    }

    function offerRecipeQuery() {
        return fetch("/offer", {
            method: "POST",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                name, selectedProducts, description, login, password
            })
        }).then(response => {
            return response.json()
        })
    }

    return (
        <Container component="main" maxWidth="xs" className={classes.root}>
            <CssBaseline />
            <div className={classes.paper}>
                <Typography className={classes.header} component="h1" variant="h5">
                    Предложите свой рецепт
                </Typography>
                <TextField variant="outlined"
                           margin="normal"
                           required
                           fullWidth
                           id="name"
                           label="Название рецепта"
                           name="name"
                           autoComplete="name"
                           autoFocus
                           onChange={(e) => setName(e.target.value)}/>
                <div style={{padding: "10px"}}/>
                <Autocomplete
                    multiple
                    id="selectedProducts"
                    options={products}
                    getOptionLabel={(option) => option}
                    onChange={(e, data) => {
                        setSelectedProducts(data);
                    }}
                    renderInput={(params) => (
                        <TextField
                            {...params}
                            variant="standard"
                            label="Продукты"
                            placeholder="Продукт"
                        />
                    )}
                    className={classes.form}
                />
                <div style={{padding: "10px"}}/>
                <TextField multiline={true}
                           onChange={(e) => setDescription(e.target.value)}
                           className={classes.form}
                           required
                           fullWidth
                           id="description"
                           label="Описание рецепта"
                           name="description"/>
                <Button type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleOfferRecipe}>
                    Предложить
                </Button>
                <div style={{padding: "10px"}}/>
                {isSuccess ?
                <Alert severity="success">
                    <AlertTitle>Успех!</AlertTitle>
                    <strong>Ваш рецепт был предложен!</strong>
                </Alert>
                    :
                <div/>}
                {isFailure ?
                <Alert severity="error">
                    <AlertTitle>Ошибка!</AlertTitle>
                    <strong>Ваш рецепт не был предложен, попробуйте позже!</strong>
                </Alert>
                    :
                <div/>}
            </div>
        </Container>
    )
}