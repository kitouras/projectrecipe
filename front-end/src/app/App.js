import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {BrowserRouter, Route, Switch} from "react-router-dom"
import Button from "@material-ui/core/Button";
import Home from "./HomeComponent";
import GetRecipes from "./GetRecipesComponent";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Login from "./LoginComponent";
import Register from "./RegisterComponent";
import ProductSet from "./ProductsSetsComponent";
import OfferRecipes from "./OfferRecipesComponent";
import ShowOfferingRecipes from "./ShowOfferingRecipesComponent";

const toolBarUseStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

function App() {

    const [products, setProducts] = React.useState(getInitProducts())

    React.useEffect(() => {
        let areThereProducts = sessionStorage.getItem("areThereProducts");
        if (areThereProducts) {
            setProducts(products)
        } else {
            getProductsMethod().then(r => {
                setProducts(r)
                sessionStorage.setItem("areThereProducts", "t")
            })
        }
        sessionStorage.setItem("products", JSON.stringify(products))
    }, [products])

    const [sets, setSets] = React.useState(getInitSets())

    React.useEffect(() => {
        sessionStorage.setItem("sets", JSON.stringify(sets))
    }, [sets])

    const [offeringRecipes, setOfferingRecipes] = React.useState(getInitOfferingRecipes())

    React.useEffect(() => {
        console.log(offeringRecipes)
        sessionStorage.setItem("offeringRecipes", JSON.stringify(offeringRecipes))
    }, [offeringRecipes])

    const [state, setState] = React.useState(sessionStorage.getItem("state") || "N")

    React.useEffect(() => {
        sessionStorage.setItem("state", state)
    }, [state])

    const [userName, setUserName] = React.useState(sessionStorage.getItem("userName") || "")

    React.useEffect(() => {
        sessionStorage.setItem("userName", userName)
    }, [userName])

    const [password, setPassword] = React.useState(sessionStorage.getItem("password") || "")

    React.useEffect(() => {
        sessionStorage.setItem("password", password)
    }, [password])

    const [role, setRole] = React.useState(sessionStorage.getItem("role") || "")

    React.useEffect(() => {
        sessionStorage.setItem("role", role)
    }, [role])

    function getInitSets() {
        let sessionSets = sessionStorage.getItem("sets")
        if (sessionSets === null) {
            return []
        } else {
            return JSON.parse(sessionSets)
        }
    }

    function getInitOfferingRecipes() {
        let sessionOfferingRecipes = sessionStorage.getItem("offeringRecipes")
        if (sessionOfferingRecipes === null) {
            return []
        } else {
            return JSON.parse(sessionOfferingRecipes)
        }
    }

    function getInitProducts() {
        let sessionProducts = sessionStorage.getItem("products")
        if (sessionProducts === null) {
            return []
        } else {
            return JSON.parse(sessionProducts)
        }
    }

    function getProductsMethod() {
        return fetch("/get_recipes").then(response => {
            return response.json()
        })
    }

    const toolBarClasses = toolBarUseStyles()

    const onClickMainPage = () => {
        window.location.href = "/"
    }

    const onClickLookSets = () => {
        window.location.replace("/look_sets")
    }

    const onClickGetRecipes = () => {
        window.location.replace("/get_recipes")
    }

    const onClickLogin = () => {
        window.location.replace("/login")
    }

    const onClickLogout = () => {
        setState("N")
        setUserName("")
        setPassword("")
        setRole("")
        setSets([])
        setOfferingRecipes([])
    }

    const onClickRegister = () => {
        window.location.replace("/register")
    }

    const onClickOfferRecipe = () => {
        window.location.replace("/offer")
    }

    const onClickShowOfferingRecipes = () => {
        window.location.replace("/show_offering_recipes")
    }

    return (
      <BrowserRouter>
          <div className={toolBarClasses.root}>
              <AppBar position="static" style={{backgroundColor: "orange"}}>
                  <Toolbar>
                      <div className={toolBarClasses.title}>
                          <Button variant={"contained"} onClick={onClickMainPage} data-testid="mainPage">Главная</Button>
                      </div>
                      {role === "M"
                          ?
                      <>
                          <Button variant={"contained"} onClick={onClickShowOfferingRecipes} data-testid="showOfferingRecipes">Посмотреть предложенные рецепты</Button>
                          <div style={{padding: "10px"}}/>
                      </>
                          :
                      <div />
                      }
                      {state === "N" ?
                      <>
                          <Button variant={"contained"} onClick={onClickRegister} data-testid="register">Зарегистрироваться</Button>
                          <div style={{padding: "10px"}}/>
                          <Button variant={"contained"} onClick={onClickLogin} data-testid="login">Авторизоваться</Button>
                      </>
                                :
                      <>
                          <Button variant={"contained"} onClick={onClickOfferRecipe} data-testid="offerRecipe">Предложить рецепт</Button>
                          <div style={{padding: "10px"}}/>
                          <Button variant={"contained"} onClick={onClickLogout} data-testid="logout">Выйти из аккаунта</Button>
                      </>
                      }
                      <div style={{padding: "10px"}}/>
                      <Button variant={"contained"} onClick={onClickGetRecipes} data-testid="getRecipes">Запросить рецепты</Button>
                      <div style={{padding: "10px"}}/>
                      <Button variant={"contained"} onClick={onClickLookSets} data-testid="lookSets">Посмотреть наборы продуктов</Button>
                      <div style={{padding: "10px"}}/>
                  </Toolbar>
              </AppBar>
          </div>
          <div>
              <Switch data-testid={"switch"}>
                  <Route exact path={"/"} data-testid={"mainRoute"}>
                      <Home/>
                  </Route>
                  <Route path={"/get_recipes"} data-testid={"getRecipeRoute"}>
                      <GetRecipes products={products} sets={sets} />
                  </Route>
                  <Route path={"/look_sets"} data-testid={"lookSetsRoute"}>
                      {state === "N" ?
                          <Login
                              setState={setState}
                              setUserName={setUserName}
                              setPassword={setPassword}
                              setRole={setRole}
                              setSets={setSets}
                              setOfferingRecipes={setOfferingRecipes}
                          />
                        :
                          <ProductSet
                              sets={sets}
                              setSets={setSets}
                              products={products}
                              login={userName}
                              password={password}/>}
                  </Route>
                  <Route path={"/login"} data-testid={"loginRoute"}>
                      {state === "N" ?
                          <Login
                              setState={setState}
                              setUserName={setUserName}
                              setPassword={setPassword}
                              setRole={setRole}
                              setSets={setSets}
                              setOfferingRecipes={setOfferingRecipes}/>
                        :
                      <Home />}
                  </Route>
                  <Route path={"/register"} data-testid={"registerRoute"}>
                      {state === "N" ?
                          <Register />
                      :
                        <Home />}
                  </Route>
                  <Route path={"/offer"} data-testid={"offerRoute"}>
                      {state === "N" ?
                          <Login
                              setState={setState}
                              setUserName={setUserName}
                              setPassword={setPassword}
                              setRole={setRole}
                              setSets={setSets}
                              setOfferingRecipes={setOfferingRecipes}/>
                      :
                      <OfferRecipes products={products} login={userName} password={password}/>}
                  </Route>
                  <Route path={"/show_offering_recipes"} data-testid={"showOfferingRecipesRoute"}>
                      {role === "M"
                          ?
                      <ShowOfferingRecipes
                          login={userName}
                          password={password}
                          offeringRecipes={offeringRecipes}
                          setOfferingRecipes={setOfferingRecipes}/>
                          :
                      <Home />}
                  </Route>
              </Switch>
          </div>
      </BrowserRouter>
  )
}

export default App;
