import React from 'react';
import {render} from '@testing-library/react';
import {unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import AddProductDialog from "../app/AddProductDialogComponent";

let container = null
beforeEach(() => {
    container = document.createElement("div")
    document.body.appendChild(container)
})

afterEach(() => {
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

it('render App.js', () => {
    const products = [{id: 1, name: "p1"}]
    const set={id: 1, name: "s", products: products}

    act(() => {
        render(<AddProductDialog products={products} set={set} isOpen={true} handleClose={jest.fn()} handleAddSet={jest.fn()} />,
            container)
    })

    const cancelButton = document.querySelector("[data-testid=cancel]")
    const addButton = document.querySelector("[data-testid=add]")
    act(() => {
        addButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })
    act(() => {
        cancelButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })

    act(() => {
        render(<AddProductDialog products={products} set={set} isOpen={false} handleClose={jest.fn()} handleAddSet={jest.fn()} />,
            container)
    })

    const cancelButton1 = document.querySelector("[data-testid=cancel]")
    const addButton1 = document.querySelector("[data-testid=add]")
    act(() => {
        addButton1.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })
    act(() => {
        cancelButton1.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })
})