import {unmountComponentAtNode} from "react-dom";
import { render } from '@testing-library/react';
import {act} from "react-dom/test-utils";
import Home from "../app/HomeComponent";
import React from "react";

let container = null
beforeEach(() => {
    container = document.createElement("div")
    document.body.appendChild(container)
})

afterEach(() => {
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

it('render Home.js', () => {
    act(() => {
        render(<Home/>, container)
    })
})