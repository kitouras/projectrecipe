import React from 'react';
import { render } from '@testing-library/react';
import {unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import App from '../app/App';

let container = null
beforeEach(() => {
  container = document.createElement("div")
  document.body.appendChild(container)
})

afterEach(() => {
  unmountComponentAtNode(container)
  container.remove()
  container = null
})

it('render App.js', () => {

  //Not login user
  act(() => {
    render(<App/>, container)
  })

  let mainPageButton = document.querySelector("[data-testid=mainPage]")
  let loginButton = document.querySelector("[data-testid=login]")
  let registerButton = document.querySelector("[data-testid=register]")
  let getRecipesButton = document.querySelector("[data-testid=getRecipes]")
  expect(window.location.href).toBe("http://localhost/")

  act(() => {
    mainPageButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
  act(() => {
    loginButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
  act(() => {
    registerButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
  act(() => {
    getRecipesButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })

  //login user
  jest.spyOn(Storage.prototype, 'getItem').mockImplementation((arg) => {
    if (arg === "state") {
      return "Y"
    } else {
      return null
    }
  })
  act(() => {
    render(<App/>, container)
  })
  let offerRecipeButton = document.querySelector("[data-testid=offerRecipe]")
  let logoutButton = document.querySelector("[data-testid=logout]")
  let lookSetsButton = document.querySelector("[data-testid=lookSets]")
  act(() => {
    offerRecipeButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
  act(() => {
    logoutButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
  act(() => {
    lookSetsButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })

  //Moderator
  jest.spyOn(Storage.prototype, 'getItem').mockImplementation((arg) => {
    if (arg === "state") {
      return "Y"
    } else if (arg === "role") {
      return "M"
    } else {
      return null
    }
  })

  act(() => {
    render(<App/>, container)
  })

  let showOfferingRecipesButton = document.querySelector("[data-testid=showOfferingRecipes]")
  act(() => {
    showOfferingRecipesButton.dispatchEvent(new MouseEvent("click", { bubbles: true}))
  })
});

// test('renders learn react link', () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });
