import React from 'react';
import {fireEvent, render} from '@testing-library/react';
import {unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import CreateSetDialog from "../app/CreateSetDialogComponent";

let container = null
beforeEach(() => {
    container = document.createElement("div")
    document.body.appendChild(container)
})

afterEach(() => {
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

it('render App.js', () => {
    const products = [{id: 1, name: "p1"}]
    act(() => {
        render(<CreateSetDialog products={products} isOpen={true} handleClose={jest.fn()} handleCreateSet={jest.fn()}/>,
            container)
    })

    const cancelButton = document.querySelector("[data-testid=cancel]")
    const addButton = document.querySelector("[data-testid=cancel]")
    act(() => {
        cancelButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })
    act(() => {
        addButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })

    const nameField = document.querySelector("[data-testid=name]")
    fireEvent.change(nameField, {target: {value: "n"}})

    act(() => {
        addButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })

    act(() => {
        render(<CreateSetDialog products={products} isOpen={false} handleClose={jest.fn()} handleCreateSet={jest.fn()}/>,
            container)
    })

})