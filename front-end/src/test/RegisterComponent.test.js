import {unmountComponentAtNode} from "react-dom";
import { render, fireEvent} from '@testing-library/react';
import {act} from "react-dom/test-utils";
import React from "react";
import { BrowserRouter as Router } from 'react-router-dom';
import Register from "../app/RegisterComponent";

let container = null
beforeEach(() => {
    container = document.createElement("div")
    document.body.appendChild(container)
})

afterEach(() => {
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

it('render Register.js', () => {

    const l1 = "l1"
    const l2 = "l2"
    const p1 = "p1"
    const p2 = "p2"
    const a1 = true
    const a2 = false
    const le1 = {
        preventDefault() {},
        target: { value: l1 }
    };
    const le2 = {
        preventDefault() {},
        target: { value: l2 }
    };
    const pe1 = {
        preventDefault() {},
        target: { value: p1 }
    };
    const pe2 = {
        preventDefault() {},
        target: { value: p2 }
    };

    jest.spyOn(global, "fetch").mockImplementation((url, conf) => {
        if (url === "/register" && conf === {
            "method": 'POST',
            "headers": {'Content-Type' : 'application/json'},
            "body": JSON.stringify({
                l1, p1
            })
        }) {
            return Promise.resolve({
                json: () => Promise.resolve(a1)
            })
        } else {
            return Promise.resolve({
                json: () => Promise.resolve(a2)
            })
        }
    })

    act(() => {
        render(<Router>
            <Register/>
        </Router>, container)
    })

    const registerButton = document.querySelector("[data-testid=registerButton]")
    const loginField = document.querySelector("[data-testid=loginField]")
    const passwordField = document.querySelector("[data-testid=passwordField]")
    fireEvent.change(loginField, le1)
    fireEvent.change(passwordField, pe1)
    act(() => {
        registerButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })

    fireEvent.change(loginField, le2)
    fireEvent.change(passwordField, pe2)
    act(() => {
        registerButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })



})