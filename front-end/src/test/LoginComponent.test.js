import {unmountComponentAtNode} from "react-dom";
import { render, fireEvent} from '@testing-library/react';
import {act} from "react-dom/test-utils";
import React from "react";
import Login from "../app/LoginComponent";
import { BrowserRouter as Router } from 'react-router-dom';

let container = null
beforeEach(() => {
    container = document.createElement("div")
    document.body.appendChild(container)
})

afterEach(() => {
    unmountComponentAtNode(container)
    container.remove()
    container = null
})

it('render Login.js', () => {

    const a1 = {login: "", p: "", role: "", state: "N"}
    const a2 = {login: "l2", p: "p2", role: "U", state: "Y"}
    const le1 = {
        preventDefault() {},
        target: { value: 'l1' }
    };
    const le2 = {
        preventDefault() {},
        target: { value: 'l2' }
    };
    const pe1 = {
        preventDefault() {},
        target: { value: 'p1' }
    };
    const pe2 = {
        preventDefault() {},
        target: { value: 'p2' }
    };

    jest.spyOn(global, "fetch").mockImplementation((url, conf) => {
        if (url === "/login?login=l1&password=p1" && conf === {
            method: "GET"
        }) {
            return Promise.resolve({
                json: () => Promise.resolve(a1)
            })
        } else {
            return Promise.resolve({
                json: () => Promise.resolve(a2)
            })
        }
    })

    act(() => {
        render(<Router>
            <Login
                setState={jest.fn()}
                setUserName={jest.fn()}
                setPassword={jest.fn()}
                setRole={jest.fn()}
                setSets={jest.fn()}
                setOfferingRecipes={jest.fn()}/>
        </Router>, container)
    })

    const loginButton = document.querySelector("[data-testid=login]")
    const loginField = document.querySelector("[data-testid=loginField]")
    const passwordField = document.querySelector("[data-testid=passwordField]")
    fireEvent.change(loginField, le1)
    fireEvent.change(passwordField, pe1)
    act(() => {
        loginButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })

    fireEvent.change(loginField, le2)
    fireEvent.change(passwordField, pe2)
    act(() => {
        loginButton.dispatchEvent(new MouseEvent("click", {bubbles: true}))
    })



})